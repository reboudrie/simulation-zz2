double simPi(int nbDraw){
    //Numbre of points that are in the circle
    double nbUnder = 0;

    for(int i=0; i<nbDraw; i++){
        double x = genrand_real1();
        double y = genrand_real1();

        //Check if the point drawed is in the circle
        if(x*x + y*y <= 1.){
            nbUnder++;
        }
    }

    //Calculate the approximation of pi using the surface of the circle
    return (nbUnder/(double)nbDraw)*4.;
}