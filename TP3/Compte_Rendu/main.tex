\documentclass{article}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{myTemplate}
\usepackage[hidelinks]{hyperref}
\usepackage{minted}
\usepackage{xcolor}
\usepackage{dingbat}
\usepackage{fancyhdr}
\usepackage[most]{tcolorbox}
\usepackage{pgfplots}
\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }
\pgfplotsset{compat=newest}
\tcbuselibrary{listings, minted, breakable}

\school{Institut Supérieur d'Informatique de Modélisation et de leurs Applications}
\lesson{Simulation}
\title{Simulation de Monte Carlo et intervalle de confiance}
\logo{./logo/logo_ISIMA.png}
\author{Rémi BOUDRIE}

\definecolor{bg}{rgb}{0.25,0.25,0.30}
\definecolor{linebg}{rgb}{0.95,0.95,0.95}

\newtcblisting{code}{
    listing engine=minted,
    minted language=C,
    minted style=monokai,
    minted options={
        breaklines,autogobble,linenos,numbersep=3mm,
        breaksymbolleft=,
        breaksymbolindentleft=0pt,
        breaksymbolsepleft=0pt,
        breaksymbolright=\textcolor{white}{\carriagereturn},
        breaksymbolindentright=0pt,
        breaksymbolsepright=0pt
    },
    left=5mm,enhanced,
    listing only,
    breakable,
    colback=bg,
    colframe=black,
    overlay={\begin{tcbclipinterior}\fill[red!8!blue!8!white] (frame.south west) rectangle ([xshift=5mm]frame.north west);\end{tcbclipinterior}}
}

\newtcbinputlisting{\codeFile}[2][]{
    listing engine=minted,%
    minted language=C,%
    minted style=monokai,%
    minted options={%
        breaklines,autogobble,linenos,numbersep=3mm,%
        breaksymbolleft=,%
        breaksymbolindentleft=0pt,%
        breaksymbolsepleft=0pt,%
        breaksymbolright=\textcolor{white}{\carriagereturn},%
        breaksymbolindentright=0pt,%
        breaksymbolsepright=0pt%
    },
    left=5mm,enhanced,%
    listing only,%
    breakable,%
    colback=bg,%
    colframe=black,%
    overlay={\begin{tcbclipinterior}\fill[red!8!blue!8!white] (frame.south west) rectangle ([xshift=5mm]frame.north west);\end{tcbclipinterior}},%
    listing file = {#2},#1%
}

\makehdft
\begin{document}

\maketitle

\tableofcontents

\newpage

\section{Introduction}

Lors du dernier TP nous avons utilisé un générateur de nombres aléatoire et nous avons crée de quoi
tirer un nombre aléatoire selon certaines règles. Nous allons lors de ce TP utiliser le travail fait
lors du précédent TP pour mettre en place notre simulation de Monte Carlo et crée un intervalle de
confiance pour cette simulation.

\section{Simulation de $\pi$}

Nous allons pour commencer faire une simulation de Monte Carlo. Pour cela nous allons créer une simulation
qui va chercher à trouver une approximation de $\pi$. Pour cela nous allons générer 2 nombres aléatoires.
Ces deux nombres aléatoires se trouvent dans l'intervalle $[ 0 ; 1]$ et ensuite nous regardons si le point
se trouve dans le cercle de centre $(0,0)$ et rayon 1. À partir de cela nous pouvons trouver la surface 
du quart de cercle qui correspond à: 

\[\frac{\text{Nombre de points dans le cercle}}{\text{Nombre totale de points}}\]
\\\noindent
Sachant que cette surface correspond au quart de cercle de rayon un, pour trouver la valeur
estimé de $\pi$ il nous suffit juste de la multiplier par 4. Ainsi nous obtenons donc le code C suivant
qui permet de faire cette simulation de Monte Carlo qui permet de trouver une approximation de $\pi$.
\\
\\
\codeFile{TP3SimPi.c}
\ \\
\\\noindent
Nous avons testé cette fonction et voici le graphique de la différence entre notre estimation de pi et 
la valeur de pi présente dans ''math.h'' en fonction du nombre de points tiré pour calculer la surface
du cercle.
\\
\\
\begin{center}
    \begin{tikzpicture}
        \begin{loglogaxis}[axis lines = left,
            xlabel={Nombre de points tirés pour calculer pi},
            ylabel={Différence entre pi simuler et pi connue}]
            \addplot[only marks, scatter, mark size=1.5pt] table [x, y, col sep=comma] {simulation_pi.csv};
        \end{loglogaxis}
    \end{tikzpicture}
\end{center}

\section{Interprétation des résultats}

Pour analyser nos résultats de simulation nous pouvons d'abord faire plusieurs fois la même
simulation. Il est important de ne pas se limiter au résultat d'une seule simulation car une simulation
peut donner un résultat proche de celui qu'on attend mais toutes les autres simulations que nous
lancerons auront des résultats différents. Le résultat qui était proche de ce que nous attendions
n'était en réalité qu'un ``coup de chance``. C'est donc pour cela de faire chaque simulation un nombre
important de fois afin d'être sûr que les résultats obtenus ne sont pas juste dû à de la chance.
Maintenant il existe plusieurs moyens d'analyser cet ensemble de résultats et nous allons donc
présenter ce que nous avons utilisé lors de ce TP.

\subsection{Moyenne}

Tout d'abord nous pouvons utiliser le calcul le plus couramment utilisé dans ce genre de
situation, la moyenne. La moyenne va nous permettre de prendre en compte l'ensemble des valeurs
que nous avons simulées pour trouver une valeur qui sera plus proche de la valeur que l'on veut.
Par exemple nous lançons 30 simulations de $\pi$ de chacune 1 000 000 000 de points, nous
avons ensuite calculé la moyenne pour ces 30 simulations. Sur le graphique suivant nous représentons
toutes les valeurs de $\pi$ trouvées et le point rouge correspond à la moyenne qui vaut $3,14158434$.

\begin{center}
    \vspace{6ex}
    \begin{tikzpicture}
        \begin{axis}[xmin=3.14145, xmax=3.14167, ymin=0.999999, ymax=1.00002,hide y axis,%
            xscale=2.25, yscale=2.25, axis lines = left,%
            xticklabels={0,1,3.14145,3.14150,3.14155,3.14160,3.14165}]
            \addplot[only marks, mark size=0.5pt] table [x, y, col sep=comma] {new_simulation_mean_pi.csv};
            \addplot[only marks,  mark size=1pt, color=red] coordinates{(3.14158434,1)};
        \end{axis}
    \end{tikzpicture}
\end{center}

Ainsi nous nous approchons d'une valeur plus proche de $\pi$ grâce à la moyenne. En effet, nous ne
trouvons qu'un écart de $0,000008316$ entre la valeur de $\pi$ connut et celle que nous avons trouvé
grâce à nos simulations. Le calcul de cette moyenne est fait avec le code C suivant:
\\
\\
\codeFile{TP3MeanAllDraw.c}

\subsection{L'intervalle de confiance}

L'autre moyen que nous avons utilisé lors de ce TP pour analyser les valeurs de nos simulations de
$\pi$ est l'intervalle de confiance. En effet lorsque nous faisons un grand nombre de simulations
nous voudrions savoir avec confiance dans quel intervalle les valeurs que nous allons tirer
ensuite peuvent se trouver. Ainsi nous allons calculer l'intervalle de confiance de notre simulation,
ainsi nous pourrons savoir que la valeur de $\pi$ à 95\% de chance de se trouver dans cet intervalle.
Nous avons donc tiré 1 000 valeurs de $\pi$ qui chacune est simulée avec 1 000 000 000 de points. Nous
avons utilisé ces valeurs afin de créer un histogramme des valeurs de $\pi$ tiré et avons noté sur celui-ci
en rouge la moyenne qui est de $3,14158976$ et l'intervalle de confiance qui est $[3,14158660 ; 3,14159291]$
en vert.

\begin{center}
\begin{tikzpicture}
    \begin{axis}[
        axis lines = left,
        xmin=3.14143716, xmax=3.14173802, xscale=2.25,
        xticklabels={0,1,3.14145,3.14150,3.14155,3.14160,3.14165,3.14170},
        %xlabel={classes},
        %ylabel={Nombre tirés en $10^8$}%,
        %nodes near coords style={font=\tiny}
        ]
        \addplot[mark=none, color=blue] table [x, y, col sep=comma] {histo_pi.csv};
        \addplot +[mark=none, color=red,dotted,line width=0.6mm] coordinates{(3.14158976,0)(3.14158976,15)};
        \addplot +[mark=none, color=green,line width=0.4mm] coordinates{(3.14158660,0)(3.14158660,15)};
        \addplot +[mark=none, color=green,line width=0.4mm] coordinates{(3.14159291,0)(3.14159291,15)};
        %\addplot +[mark=none, color=black,line width=0.4mm] coordinates{(3.14159265,0)(3.14159265,15)};
    \end{axis}
\end{tikzpicture}
\end{center}

Le code C qui permet de calculer cet intervalle de confiance est le suivant:
\\
\\
\codeFile{TP3ConfidenceInterval.c}

\section{Conclusion}

Ainsi lors de ce TP nous avons montré avec la méthode de Monte Carlo que nous pouvons approcher
la valeur de $\pi$. Nous avons utilisé la moyenne pour obtenir une valeur proche de la valeur
possible de $\pi$. Avec cela nous savons également qu'il y a 95\% de chances pour que la valeur
$\pi$ se trouve dans l'intervalle de confiance que nous avons cité plus tôt. Ainsi nous avons
montré et utilisé des moyens pour d'utilisé des simulations afin d'approximer des valeurs
du monde réel.

\end{document}