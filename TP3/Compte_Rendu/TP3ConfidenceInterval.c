double * confidenceInterval(double * draws, int nbDraw){
    // Calculate the mean
    double mean = meanAllDraw(draws, nbDraw);
    // Calculate the variance
    double var = varAllDraw(draws, nbDraw, mean);

    // Get the right value of the law student depending on the number of draw of pi
    double student;
    if(nbDraw == 40){
        student = 2.021;
    }else if(nbDraw == 80){
        student = 2.000;
    }else if(nbDraw == 120){
        student = 1.980;
    }else if(nbDraw == 1000){
        student = 1.962;
    }else{
        student = student95[nbDraw-1];
    }

    // Calculate the confidence radius
    double R = student*sqrt(var/(double) nbDraw);

    // Create and fill the confidence interval
    double * interval = calloc(2, sizeof(double));
    interval[0] = mean - R;
    interval[1] = mean + R;
    return interval;
}