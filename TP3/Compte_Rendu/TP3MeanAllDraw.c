double meanAllDraw(double * draws, int nbDraw){
    double mean = 0;

    for(int i=0; i<nbDraw; i++){
        mean += draws[i];
    }

    return mean/((double) nbDraw);
}