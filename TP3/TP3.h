#ifndef __TP3_H__
#define __TP3_H__

/*Values of the students laws with 95% confidence for n experiments*/
extern double student95[];

/**--------------------------------------------------------------
 * @brief               return an evaluation of pi that
 *                      is approximated by nbDraw random
 *                      numbers
 * 
 * @param   nbDraw      the number of draw that needs to be done
 * @return  double      the approxiamtion of pi
 *
 ---------------------------------------------------------------*/
double simPi(int nbDraw);

/**--------------------------------------------------------------
 * @brief                   Create a table of the size of nbDraw and
 *                          for each draw run simPi with nbDrawEach each
 *                          time 
 * 
 * @param   nbDraw          Size of the table
 * @param   nbDrawEach      Number of draws for each simPi run
 * @return  double*         Table of all the simPi draw
 * 
 ---------------------------------------------------------------*/
double * drawSimPi(int nbDraw, int nbDrawEach);

/**--------------------------------------------------------------
 * @brief               Calculate the mean of the table draws that has
 *                      a size of nbDraw
 * 
 * @param   draws       Table of values for calculating the mean of
 * @param   nbDraw      Size of the table of values
 * @return  double      Mean of the table
 * 
 ---------------------------------------------------------------*/
double meanAllDraw(double * draws, int nbDraw);

/**--------------------------------------------------------------
 * @brief               Calculate the variance without bias of the 
 *                      table draws that has a size of nbDraw
 * 
 * @param   draws       Table of values for calculating the variance 
 *                      without bias of
 * @param   nbDraw      Size of the table of values
 * @param   mean        Mean value of this table
 * @return  double      Variance without bias of the table
 * 
 ---------------------------------------------------------------*/
double varAllDraw(double * draws, int nbDraw, double mean);

/**--------------------------------------------------------------
 * @brief               Return the confidence interval of 95% of the
 *                      table draws that has a size of nbDraw
 * 
 * @param   draws       Table of values for calculating the confidence 
 *                      interval of 95% of
 * @param   nbDraw      Size of the table of values
 * @return  double*     Confidence interval of 95% of the table
 * 
 ---------------------------------------------------------------*/
double * confidenceInterval(double * draws, int nbDraw);

#endif