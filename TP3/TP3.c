#include "TP3.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mt19937ar.h"
#include "tp2.h"

double student95[] = {12.706, 4.303, 3.182, 
                        2.776, 2.571, 2.447,
                        2.365, 2.308, 2.262,
                        2.228, 2.201, 2.179,
                        2.160, 2.145, 2.131,
                        2.120, 2.110, 2.101,
                        2.0193, 2.086, 2.080,
                        2.074, 2.069, 2.064,
                        2.060, 2.056, 2.052,
                        2.048, 2.045, 2.042};

double simPi(int nbDraw){
    //Numbre of points that are in the circle
    double nbUnder = 0;

    for(int i=0; i<nbDraw; i++){
        double x = genrand_real1();
        double y = genrand_real1();

        //Check if the point drawed is in the circle
        if(x*x + y*y <= 1.){
            nbUnder++;
        }
    }

    //Calculate the approximation of pi using the surface of the circle
    return (nbUnder/(double)nbDraw)*4.;
}

double * drawSimPi(int nbDraw, int nbDrawEach){
    //Create the table for all the draws that needs to be done
    double * allPi = calloc(nbDraw, sizeof(double));

    //Check if the table has been created
    if(!allPi){
        fprintf(stderr, "Can't allocate the table of %d int", nbDraw);
        exit(EXIT_FAILURE);
    }

    fprintf(stderr, "Start draw all simPi:\n");

    //Loop of all Pi that needs to be draw
    for(int i=0; i<nbDraw; i++){
        fprintf(stderr,"%d/%d\n", i, nbDraw);
        allPi[i] = simPi(nbDrawEach);
    }

    return allPi;
}

double meanAllDraw(double * draws, int nbDraw){
    double mean = 0;

    for(int i=0; i<nbDraw; i++){
        mean += draws[i];
    }

    return mean/((double) nbDraw);
}

double varAllDraw(double * draws, int nbDraw, double mean){
    double var = 0;

    for(int i=0; i<nbDraw; i++){
        var += (draws[i] - mean) * (draws[i] - mean);
    }

    return var/((double) nbDraw - 1.);
}

double * confidenceInterval(double * draws, int nbDraw){
    // Calculate the mean
    double mean = meanAllDraw(draws, nbDraw);
    // Calculate the variance
    double var = varAllDraw(draws, nbDraw, mean);

    // Get the right value of the law student depending on the number of draw of pi
    double student;
    if(nbDraw == 40){
        student = 2.021;
    }else if(nbDraw == 80){
        student = 2.000;
    }else if(nbDraw == 120){
        student = 1.980;
    }else if(nbDraw == 1000){
        student = 1.962;
    }else{
        student = student95[nbDraw-1];
    }

    // Calculate the confidence radius
    double R = student*sqrt(var/(double) nbDraw);

    // Create and fill the confidence interval
    double * interval = calloc(2, sizeof(double));
    interval[0] = mean - R;
    interval[1] = mean + R;
    return interval;
}