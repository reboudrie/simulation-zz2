#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mt19937ar.h"
#include "tp2.h"
#include "TP3.h"

int main(int argc, char * argv[]){

    // initialise the random number generator
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    /*double pas = 1. + (10.)/100.;
    for(double i=1000; i<=1000000000; i*=pas){
        double mean = 0.;
        for(int j=0; j<100; j++){
            mean += simPi((int) i);
        }
        mean = mean/100.;
        fprintf(stdout, "%d, %.6f\n",(int) i, M_PI - mean);
        fprintf(stderr, "%d, ", (int) i);
    }*/

    int nbDrawMean = 1000, inIterv = 0;
    double * allPiMean = drawSimPi(nbDrawMean,1000000000), min=10., max=0.;
    double mean = meanAllDraw(allPiMean,nbDrawMean);
    double * interval = confidenceInterval(allPiMean,nbDrawMean);
    for(int i=0; i<nbDrawMean; i++){
        fprintf(stdout, "%.09f,1\n",allPiMean[i]);
        if(allPiMean[i] < min){
            min = allPiMean[i];
        }
        if(allPiMean[i] > max){
            max = allPiMean[i];
        }
        if(allPiMean[i] <= interval[1] && allPiMean[i] >= interval[0]){
            inIterv++;
        }
    }
    fprintf(stderr,"\nMin: %.08f, Max:%.08f\n", min, max);
    fprintf(stderr,"Moyenne: %.08f, différence avec pi: %.09f\n", mean, M_PI-mean);
    fprintf(stderr,"Intervalle de confiance à 95%% [%.08f ; %.08f]\n",interval[0],interval[1]);
    fprintf(stderr,"%d/%d sont dans l'intervalle de confiance soit %.02f%%\n",inIterv,nbDrawMean,((double)inIterv/(double)nbDrawMean) * 100.);
    int sizeHisto = 250;
    unsigned int * histo = calloc(sizeHisto,sizeof(unsigned int));
    for(int i=0; i<nbDrawMean; i++){
        histo[(int) ((allPiMean[i]-min)/((double)(max-min)/(double)sizeHisto))]++;
    }
    fprintf(stdout,"\n");
    for(int i=0; i<sizeHisto; i++){
        fprintf(stdout, "%.08f,%d\n", min+((double)(max-min)/(double)sizeHisto)*i, histo[i]);
    }

    free(histo);
    free(allPiMean);

    //Intervalle de confiance
    /*
    double * allPi = drawSimPi(10, 10000000);
    double * inter = confidenceIntervalPI(allPi, 10);
    free(allPi);
    
    fprintf(stdout, "intervalle de confiance: [ %f; %f ]\n", inter[0], inter[1]);
    free(inter);*/
}