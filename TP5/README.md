Cette simulation fonctionne avec ncurses afin de pouvoir gèrer
les couleurs et permettre d'également garder un affichage propre même si la
fenêtre du terminal est rapetissé. Si la taille du terminal est plus petit
que le contenu que l'on veut afficher des flèches apparaîtrons sur les bords
où le contenu a été coupé. Il suffira d'utiliser les flèches directionnelles
pour aller parcourir le contenu.

Afin de quitter le programme sans problème il faut appuyer sur F1.

Des fois ncurses semble être capricieux, si jamais cela arrive forcer à tout
recompiler semble régler les problèmes. 