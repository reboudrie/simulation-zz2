#include <stdio.h>
#include <stdlib.h>

#include "Grille.h"

char ** elemColor;
char * elemValue;

void initElemColor(){
    elemColor = calloc(LAST_ELEM, sizeof(char*));
    for(int i=0; i<LAST_ELEM; i++){
        elemColor[i] = calloc(7, sizeof(char));
    }

    elemColor[Empty] = "\0";
    elemColor[Human] = FG_GREEN;
    elemColor[Robot] = FG_YELLOW;
}

void initElemValue(){
    elemValue = calloc(LAST_ELEM, sizeof(char));
    elemValue[Empty] = '.';
    elemValue[Human] = 'H';
    elemValue[Robot] = 'R';
}

void initAllGrille(){
    initElemColor();
    initElemValue();
}

coordonee createCoordonee(int x, int y){
    coordonee c;
    c.x = x;
    c.y = y;
    return c;
}

grille * createGrille(int height, int width){
    grille * gr = calloc(1, sizeof(grille));
    gr->size.height = height;
    gr->size.width = width;
    gr->grille = calloc(height, sizeof(char *));

    for(int i=0; i<height; i++){
        gr->grille[i] = calloc(width, sizeof(char));
        for(int j=0; j<width; j++){
            gr->grille[i][j] = '.';
        }
    }

    return gr;
}

void printElemColor(FILE * file, char elem){
    int pos = 0;
    while(elem != elemValue[pos]){
        pos++;
    }
    fprintf(file, "%s", elemColor[pos]);
}

void printGrille(FILE * file, grille * gr){
    for(int i=0; i<gr->size.height; i++){
        for(int j=0; j<gr->size.width; j++){
            printElemColor(file, gr->grille[i][j]);
            fprintf(file, "%c%s ", gr->grille[i][j],RESET_COLOR);
        }
        fprintf(file, "\n");
    }
}

void addHuman(grille * gr, coordonee coord){
    if(coord.x<gr->size.width && coord.y<gr->size.height){
        gr->grille[coord.y][coord.x] = elemValue[Human];
    }
}

void addRobot(grille * gr, coordonee coord){
    if(coord.x<gr->size.width && coord.y<gr->size.height){
        gr->grille[coord.y][coord.x] = elemValue[Robot];
    }
}

coordonee moveElem(grille * gr, coordonee coord, coordonee incr){
    if(coord.x<gr->size.width && coord.y<gr->size.height){
        char valeurElem = gr->grille[coord.y][coord.x];
        coordonee newPos = createCoordonee((coord.x+incr.x+gr->size.width)%gr->size.width, (coord.y+incr.y+gr->size.height)%gr->size.height);
        if(elemValue[Empty] == gr->grille[newPos.y][newPos.x]){
            gr->grille[coord.y][coord.x] = '.';
            gr->grille[newPos.y][newPos.x] = valeurElem;
        
            return newPos;
        }
    }

    return coord;
}

coordonee movementRobot(grille * gr, coordonee Robot){
    coordonee vect;
    if(Robot.x == 0){
        vect = createCoordonee(-1, -1);
    }else{
        vect = createCoordonee(-1, 0);
    }

    return moveElem(gr, Robot, vect);
}

coordonee movementHuman(grille * gr, coordonee Human){
    coordonee vect;
    if(Human.y%2 == 0){
        vect = createCoordonee(2, 1);
    }else{
        vect = createCoordonee(2, -1);
    }

    return moveElem(gr, Human, vect);
}