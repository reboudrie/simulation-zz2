#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "Grille.h"

int main(){
    initAllGrille();
    grille * gr = createGrille(20, 20);
    coordonee Human1 = createCoordonee(1, 1);
    coordonee Human2 = createCoordonee(0, 4);
    coordonee Robot = createCoordonee(4, 4);
    addHuman(gr, Human1);
    addHuman(gr, Human2);
    addRobot(gr, Robot);

    int nbIt = 100;
    for(int i=0; i<nbIt; i++){
        system("clear");
        printf("Human1: (%d,%d) Human2: (%d,%d) Robot: (%d,%d)\n\n",Human1.x,Human1.y,Human2.x,Human2.y,Robot.x,Robot.y);
        printGrille(stdout, gr);

        Human1 = movementHuman(gr, Human1);
        Human2 = movementHuman(gr, Human2);
        Robot = movementRobot(gr, Robot);

        usleep(250000);
    }

    return 0;
}