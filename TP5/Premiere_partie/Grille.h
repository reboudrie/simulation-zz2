#ifndef __GRILLE_H__
#define __GRILLE_H__

#include <stdio.h>

#define RESET_COLOR "\033[0m"

#define BG_BLACK "\033[30m"
#define BG_RED "\033[101m"
#define BG_GREEN "\033[102m"
#define BG_YELLOW "\033[103m"
#define BG_BLUE "\033[104m"
#define BG_WHITE "\033[107m"

#define FG_BLACK "\033[40m"
#define FG_RED "\033[91m"
#define FG_GREEN "\033[92m"
#define FG_YELLOW "\033[93m"
#define FG_BLUE "\033[94m"
#define FG_WHITE "\033[97m"

typedef enum elem{
    Empty,
    Human,
    Robot,

    LAST_ELEM
} elem;

typedef struct coordonee{
    int x;
    int y;
} coordonee;

typedef struct size{
    int height;
    int width;
} size;

typedef struct grille{
    size size;
    char ** grille;
} grille;

extern char * elemValue;
extern char ** elemColor;

void initAllGrille();

coordonee createCoordonee(int x, int y);

grille * createGrille(int height, int width);

void printElemColor(FILE * file, char elem);

void printGrille(FILE * file, grille * gr);

void addHuman(grille * gr, coordonee coord);

void addRobot(grille * gr, coordonee coord);

coordonee moveElem(grille * gr, coordonee coord, coordonee incr);

coordonee movementRobot(grille * gr, coordonee Robot);

coordonee movementHuman(grille * gr, coordonee Human);

#endif