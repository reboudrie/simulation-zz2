#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "keyFrame.h"
#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "terminalUI.h"

keyList * initKeyList(Frame * f, int * sizeOfStates, int nbOfState){
    keyList * keys = calloc(1, sizeof(keyList));

    keys-> frame = f;
    keys->currentState = 0;
    keys->numberOfStates = nbOfState;
    keys->sizeOfStates = calloc(nbOfState, sizeof(int));
    keys->keyListState = calloc(nbOfState, sizeof(int *));
    keys->keyDescription = calloc(nbOfState, sizeof(char **));
    keys->keyDescriptionSize = calloc(nbOfState, sizeof(int *));
    keys->functionForKey = calloc(nbOfState, sizeof(keyFunction *));
    keys->inputFunctionForKey = calloc(nbOfState, sizeof(void **));
    keys->freeingInputFuctions = calloc(nbOfState, sizeof(char *));

    for(int state = 0; state<nbOfState; state++){
        keys->sizeOfStates[state] = sizeOfStates[state];
        keys->keyListState[state] = calloc(sizeOfStates[state], sizeof(int));
        keys->keyDescription[state] = calloc(sizeOfStates[state], sizeof(char *));
        keys->keyDescriptionSize[state] = calloc(sizeOfStates[state], sizeof(int));
        keys->functionForKey[state] = calloc(sizeOfStates[state], sizeof(keyFunction));
        keys->inputFunctionForKey[state] = calloc(sizeOfStates[state], sizeof(void *));
        keys->freeingInputFuctions[state] = calloc(sizeOfStates[state], sizeof(char));
    }

    return keys;
}

void freeKeyList(keyList ** keyl, boolean freeingFrame){

    // Libère la frame qui contient la liste des touches de clavier
    if(freeingFrame){
        freeFrame((void **) &(*keyl)->frame);
    }

    for(int state = 0; state<(*keyl)->numberOfStates; state++){
        for(int keyPos=0; keyPos<(*keyl)->sizeOfStates[state]; keyPos++){
            if((*keyl)->freeingInputFuctions[state][keyPos]){
                free((*keyl)->inputFunctionForKey[state][keyPos]);
            }
            free((*keyl)->keyDescription[state][keyPos]);
        }

        free((*keyl)->keyListState[state]);
        free((*keyl)->keyDescription[state]);
        free((*keyl)->keyDescriptionSize[state]);
        free((*keyl)->functionForKey[state]);
        free((*keyl)->inputFunctionForKey[state]);
        free((*keyl)->freeingInputFuctions[state]);
    }

    free((*keyl)->freeingInputFuctions);
    free((*keyl)->inputFunctionForKey);
    free((*keyl)->functionForKey);
    free((*keyl)->keyDescriptionSize);
    free((*keyl)->keyDescription);
    free((*keyl)->keyListState);
    free((*keyl)->sizeOfStates);
    free((*keyl));

    *keyl = NULL;
}

void addKey(keyList * keyl, int state, int keyPosInState, int key, 
            char * keyDescription, int descriptionSize, keyFunction function,
            void * inputFunction, boolean freeingInputFunction){

    keyl->keyListState[state][keyPosInState] = key;
    keyl->keyDescription[state][keyPosInState] = calloc(descriptionSize, sizeof(char));
    keyl->keyDescriptionSize[state][keyPosInState] = descriptionSize;
    keyl->functionForKey[state][keyPosInState] = function;
    keyl->inputFunctionForKey[state][keyPosInState] = inputFunction;
    keyl->freeingInputFuctions[state][keyPosInState] = freeingInputFunction;

    for(int i=0; i<descriptionSize; i++){
        keyl->keyDescription[state][keyPosInState][i] = keyDescription[i];
    }

}

size getKeyFrameNewSize(keyList * keyl){
    size s = {1, 0};

    for(int i=0; i<keyl->sizeOfStates[keyl->currentState]; i++){
        s.width += keyl->keyDescriptionSize[keyl->currentState][i] + 3;
    }
    s.width -= 3;

    return s;
}

void updateKeyFrame(keyList * keyl){
    (void) keyl;
    //Cette fonction à un contenu en commentaire car dans la version actuel
    //on affiche pas la liste des touches possible dans une frame

    // Supression de l'ancien contenu et création d'un nouveau avec la nouvelle dimension
    /*delwin(keyl->frame->content);
    size updatedSize = getKeyFrameNewSize(keyl);
    keyl->frame->content = newpad(updatedSize.height, updatedSize.width);
    keyl->frame->contentSize.height = updatedSize.height;
    keyl->frame->contentSize.width = updatedSize.width;
    keyl->frame->showPosInContent.x = 0;
    keyl->frame->showPosInContent.y = 0;
    if(keyl->frame->contentShowSize.height > keyl->frame->contentSize.height){
        keyl->frame->contentShowSize.height = keyl->frame->contentSize.height;
    }
    if(keyl->frame->contentShowSize.width > keyl->frame->contentSize.width){
        keyl->frame->contentShowSize.width = keyl->frame->contentSize.width;
    }

    mvwprintw(stdscr, 0, 0, "%d %d %d", keyl->frame->contentSize.height, keyl->frame->showPosInContent.y, keyl->frame->contentShowSize.height);

    // écriture du nouveau du nouveau contenu
    int posX = 0;
    for(int keyPos=0; keyPos<keyl->sizeOfStates[keyl->currentState]; keyPos++){

        mvwprintw(keyl->frame->content, 0, posX, "%s", keyl->keyDescription[keyl->currentState][keyPos]);
        posX += keyl->keyDescriptionSize[keyl->currentState][keyPos]-1;
        if(keyPos < keyl->sizeOfStates[keyl->currentState]-1){
            mvwprintw(keyl->frame->content, 0, posX, " │ ");
            posX += 3;
        }
    }

    printFrame(keyl->frame);*/
}

void checkKeys(keyList ** keyl, int key){

    int keyPos = 0;
    for(keyPos = 0; keyPos<(*keyl)->sizeOfStates[(*keyl)->currentState] && 
    key!=(*keyl)->keyListState[(*keyl)->currentState][keyPos]; keyPos++);

    if(keyPos<(*keyl)->sizeOfStates[(*keyl)->currentState]){
        (*keyl)->functionForKey[(*keyl)->currentState][keyPos](keyl, (*keyl)->inputFunctionForKey[(*keyl)->currentState][keyPos]);
    }

    //Attention ne pas ajouter une interaction avec keyl dans le cas où
    //le programme c'est déjà arrêté
}