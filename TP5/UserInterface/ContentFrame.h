#ifndef __CONTENT_FRAME_H__
#define __CONTENT_FRAME_H__

#include <ncurses.h>

#include "../DefaultLib/boolean.h"
#include "Frame.h"

// Ajouter la position du contenut dans la winBox
typedef struct ContentFrame{
    Frame * f;

    WINDOW * content;
    size contentSize;

    WINDOW * winBox;
    size winBoxSize;

    size contentShowSize;
    coordinate showPosInContent;
    size spacingWithBox;
    coordinate posInBox;

    verticalPosition verticalPostionInTheBox;
    horizontalPosition horizontalPositionInTheBox;
} ContentFrame;


ContentFrame * createContentFrame(int frameId, coordinatePercent framePosInPercent, 
                                  sizePercent frameSizeInPercent, size contentSize, 
                                  size spacingWithBox, verticalPosition verticalPostionInTheBox, 
                                  horizontalPosition horizontalPositionInTheBox);

void freeContentFrame(void ** contentFrame);

void printContentFrame(void * contentFrame);

Frame * getContentFrameFrame(void * contentFrame);

void updateSizeContentFrame(void * contentFrame);

boolean isContentFrameSelected(void * contentFrame, int key);

void unselectContentFrame(void * contentFrame);

void inputKeyContentFrame(void * contentFrame, int key);


void printContentFrameBox(ContentFrame * contFrame);

#endif