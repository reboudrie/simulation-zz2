#ifndef __KEY_FRAME_H__
#define __KEY_FRAME_H__

#include <ncurses.h>

#include "../DefaultLib/boolean.h"
#include "Frame.h"

typedef struct keyList keyList;

typedef void (*keyFunction)(keyList **, void *);

struct keyList{
    Frame * frame;

    int currentState;
    int numberOfStates;
    int * sizeOfStates;
    int ** keyListState;

    char *** keyDescription;        //Mettre le nom de la touche entre parenthèse
    int ** keyDescriptionSize;

    keyFunction ** functionForKey;
    void *** inputFunctionForKey;
    char ** freeingInputFuctions;
};

keyList * initKeyList(Frame * f, int * sizeOfStates, int nbOfState);

void freeKeyList(keyList ** keyl, boolean freeingFrame);

void addKey(keyList * keyl, int state, int keyPosInState, int key, 
            char * keyDescription, int descriptionSize, keyFunction function,
            void * inputFunction, boolean freeingInputFunction);

size getKeyFrameNewSize(keyList * keyl);

void updateKeyFrame(keyList * keyl);

void checkKeys(keyList ** keyl, int key);

#endif