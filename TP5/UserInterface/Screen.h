#ifndef __SCREEN_H__
#define __SCREEN_H__

#include "../DefaultLib/boolean.h"

#define DEFAULT_SCREEN_SIZE 8

typedef enum frameType{
    contentFrame,
    //tableFrame, //Suite à un manque de temps et de
                  //nombreux problème cette fonctionnalité
                  //n'est pas implémenté

} frameType;

typedef struct Screen{
    void ** allFrames;              // Tableau de pointeur sur toutes les Frame
                                    // présente dans un Screen
    frameType * allFramesType;      // Tbleau de qui contient le type de chaque
                                    // Frame présent dans le tableau allFrames
    int sizeAllFrames;              // Taille que les tableaux font

    int nbFrames;                   // Nombre de Frame actuellement stockées
                                    // dans les tableaux
} Screen;


Screen * initScreen();


void freeScreen(Screen ** scr, boolean freeAllFrames);


void addFrameToScreen(Screen ** scr, void * frame, frameType type);


void delFrameFromScreen(Screen * scr, void * frame, boolean doFreeFrame);


void printScreen(Screen * scr);


void screenCheckAndUpdate(Screen * scr, int key);

#endif