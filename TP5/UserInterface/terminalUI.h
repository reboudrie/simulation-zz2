#ifndef __TERMINALUI_H__
#define __TERMINALUI_H__

#include <ncurses.h>

#include "../DefaultLib/position.h"
#include "../DefaultLib/boolean.h"
#include "color.h"

#define DEFAULT_SCREEN_SIZE 8
/*
typedef struct screen screen;

typedef struct frame{
    screen ** screen;
    int frameId;
    coordinate framePos;
    boolean isFrameSelected: 1;
    
    WINDOW * content;
    size contentSize;

    WINDOW * winBox;
    size winBoxSize;

    size contentShowSize;
    coordinate showPosInContent;
    size spacingWithBox;

    coordinatePercent framePosScreen;
    sizePercent frameSizeScreen;
} frame;

struct screen{
    frame ** allFrames;
    int sizeAllFrames;
    int nbFrames;
};

typedef struct leftInformations{
    frame * frame;

} leftInformations;

typedef struct rightInformations{
    frame * frame;

} rightInformations;

typedef struct game{
    frame * frame;
    grille * grille;

} game;*/


void initTerminalUI();
void freeTerminalUI();/*
void initAllColors();
void freeAllColors();
frame * createFrame(int fIndex, coordinatePercent coord, sizePercent sizeP, size contentSize, size windowSpacing);
void freeFrame(frame ** f);
void printFrame(frame * f);
void updateSizeFrame(frame * f);
boolean isFrameSelect(frame * f, int key);
void unselectFrame(frame * f);
void inputKeyFrame(frame * f, int key);
screen * initScreen();
void freeScreen(screen ** scr, boolean freeAllFrames);
void addFrameToScreen(screen ** scr, frame * f);
void delFrameFromScreen(screen * scr, frame * f, boolean freeFrame);
void screenCheckAndUpdate(screen * scr, int key);
void printScreen(screen * scr);*/

void testInter();

#endif