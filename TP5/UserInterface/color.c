#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "color.h"

int ** allColors;


/*ncurses défini les couleurs entre 0 et 1000 donc cette fonction converti
les couleurs qui sont de 0 à 255 en couleurs pour ncurses*/
void init_color_RGG(short color, short red, short green, short blue){
    double factor = 1000./255.;

    init_color(color, ((double)red)*factor, ((double)green)*factor, 
               ((double)blue)*factor);
}

void brokenColorChoose(int bg, int fg, short * realBg, short * realFg){

    int test = fg;
    short * out = realFg;

    for(int i=0; i < 2; i++){
        if(i == 1){
            test = bg;
            out = realBg;
        }

        switch (test) {
            case C_GrassDay:
                *out = 34;
                break;
            case C_GrassNight:
                *out = 29;
                break;
            case C_DirtDay:
                *out = 136;
                break;
            case C_DirtNight:
                *out = 59;
                break;
            case C_SheepDay:
                *out = 254;
                break;
            case C_SheepNight:
                *out = 104;
                break;
            case C_WolfDay:
                *out = 247;
                break;
            case C_WolfNight:
                *out = 60;
                break;
            default:
                break;
        }
    }
}

void initAllColors(){

    // Initialise l'utilisation des couleurs pour ncurses
    start_color();

    // Limite le noumbre de couleurs au max que le terminal autorise
    int size = NB_COLORS;
    if(NB_COLORS > COLORS){
        size = COLORS;
    }

    allColors = calloc(size, sizeof(int *));
    
    // Déclaration de toutes les nouvelles couleurs qui ne sont pas par défaut
    // Malheuresement même si le terminal est reconnu comme pouvant changer
    // la palette de couleur cela ne marche pas toujours et par conséquent
    // afin d'assurer un résultat convenable pour tout le monde
    // le couleurs ont été sélectionné dans la palette 256 couleurs
    // de base 
    /*init_color_RGG(C_GrassDay, 46, 130, 31);
    init_color_RGG(C_DirtDay, 130, 82, 31);
    init_color_RGG(C_SheepDay, 227, 227, 227);
    init_color_RGG(C_WolfDay, 124, 124, 124);
    init_color_RGG(C_ShepherdDay, 191, 126, 88);

    init_color_RGG(C_GrassNight, 25, 74, 62);
    init_color_RGG(C_DirtNight, 68, 53, 58);
    init_color_RGG(C_SheepNight, 109, 115, 143);
    init_color_RGG(C_WolfNight, 64, 69, 95);
    init_color_RGG(C_ShepherdNight, 96, 72, 83);*/



    // Initialise le tableau de toutes les couleurs possibles
    for(int bg=0; bg<size; bg++){
        allColors[bg] = calloc(size, sizeof(int));
        for(int fg=0; fg<size; fg++){
            short realfg = fg, realbg = bg;
            brokenColorChoose(bg, fg, &realbg, &realfg);

            allColors[bg][fg] = bg*size + fg + 1;
            init_pair(allColors[bg][fg], realfg, realbg);
        }
    }

}


void freeAllColors(){

    // Limite le noumbre de couleurs au max que le terminal autorise
    int size = NB_COLORS;
    if(NB_COLORS > COLORS){
        size = COLORS;
    }

    // Libère le tableau de toutes les couleurs possible
    for(int bg=0; bg<size; bg++){
        free(allColors[bg]);
        allColors[bg] = NULL;
    }
    free(allColors);
    allColors = NULL;

    // Libère toutes les paires de couleur de la table de couleur du terminal
    reset_color_pairs();
}


int getReverseColor(int baseColor){

    int lineNumber = (baseColor-1)/NB_COLORS;
    int columnNumber = (baseColor-1)%NB_COLORS;

    if(NB_COLORS > COLORS){
        lineNumber = (baseColor-1)/COLORS;
        columnNumber = (baseColor-1)%COLORS;
    }

    return allColors[columnNumber][lineNumber];
}