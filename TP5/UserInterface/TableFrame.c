#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "Frame.h"
#include "color.h"
#include "TableFrame.h"

TableFrame * createTableFrame(int frameId, coordinatePercent framePosInPercent, 
                              sizePercent frameSizeInPercent, size tableSize){
    
    TableFrame * tbleFrame = calloc(1, sizeof(TableFrame));
    tbleFrame->f = createFrame(frameId, framePosInPercent, frameSizeInPercent);
    tbleFrame->f->frameIdPosInFrame.x = 2;
    tbleFrame->f->frameIdPosInFrame.y = 0;
    tbleFrame->f->arrowSpacing = 0;

    copySize(&tableSize, &tbleFrame->tableSize);
    tbleFrame->posInTable.x = 0;
    tbleFrame->posInTable.y = 0;
    copySize(&tbleFrame->f->frameSize, &tbleFrame->sizeTableShown);

    tbleFrame->tableCellSize = 
        calloc(tableSize.height, sizeof(size *));
    tbleFrame->allContent = 
        calloc(tableSize.height, sizeof(cellContent *));
    tbleFrame->tableCellSpacingSize = 
        calloc(tableSize.height, sizeof(size *));
    tbleFrame->tableCellVerticalPosition = 
        calloc(tableSize.height, sizeof(verticalPosition *));
    tbleFrame->tableCellHorizontalPosition = 
        calloc(tableSize.height, sizeof(horizontalPosition *));
    if(tableSize.height > tableSize.width){
        tbleFrame->maxCellSize = calloc(tableSize.height, sizeof(size));
    }else{
        tbleFrame->maxCellSize = calloc(tableSize.width, sizeof(size));
    }
    tbleFrame->allLabels = calloc(4, sizeof(TableLabel));

    for(int i=0; i<tableSize.height; i++){
        tbleFrame->tableCellSize[i] = 
            calloc(tableSize.width, sizeof(size));
        tbleFrame->allContent[i] = 
            calloc(tableSize.width, sizeof(cellContent));
        tbleFrame->tableCellSpacingSize[i] = 
            calloc(tableSize.width, sizeof(size));
        tbleFrame->tableCellVerticalPosition[i] = 
            calloc(tableSize.width, sizeof(verticalPosition));
        tbleFrame->tableCellHorizontalPosition[i] = 
            calloc(tableSize.width, sizeof(horizontalPosition));
    }

    return tbleFrame;
}

size getRealSize(char * content, int contentSize){
    size realContentSize = {1,1};
    int curWidth = 0;
    for(int i=0; i<contentSize; i++){
        if(content[i] == '\n'){
            if(realContentSize.width < curWidth){
                realContentSize.width = curWidth;
            }
            curWidth = 0;
            realContentSize.height++;
        } else if(content[i] != '\0'){
            curWidth++;
        }
    }
    if(realContentSize.width == 1 && curWidth > 1){
        realContentSize.width = curWidth;
    }    

    return realContentSize;
}

void createCellTableFrame(TableFrame * tbleFrame, coordinate posInTable, 
                          char * content, int contentSize, int color, 
                          verticalPosition vertPos, horizontalPosition horPos,
                          size spacing){
    
    if(posInTable.x < tbleFrame->tableSize.width && posInTable.x >= 0 &&
       posInTable.y < tbleFrame->tableSize.height && posInTable.y >= 0){

        copySize(&spacing, 
            &tbleFrame->tableCellSpacingSize[posInTable.y][posInTable.x]);
        tbleFrame->tableCellHorizontalPosition[posInTable.y][posInTable.x] = 
            horPos;
        tbleFrame->tableCellVerticalPosition[posInTable.y][posInTable.x] = 
            vertPos;
        
        size realContentSize = getRealSize(content, contentSize);
        realContentSize.height += 
            tbleFrame->tableCellSpacingSize[posInTable.y][posInTable.x].height;
        if(tbleFrame->tableCellVerticalPosition[posInTable.y][posInTable.x] == centerVert){
            realContentSize.height += 
                tbleFrame->tableCellSpacingSize[posInTable.y][posInTable.x].height;
        }
        realContentSize.width += 
            tbleFrame->tableCellSpacingSize[posInTable.y][posInTable.x].width;
        if(tbleFrame->tableCellHorizontalPosition[posInTable.y][posInTable.x] == centerHori){
            realContentSize.width += 
                tbleFrame->tableCellSpacingSize[posInTable.y][posInTable.x].width;
        }
        copySize(&realContentSize,
                 &tbleFrame->tableCellSize[posInTable.y][posInTable.x]);

        if(tbleFrame->maxCellSize[posInTable.y].height < realContentSize.height){
           tbleFrame->maxCellSize[posInTable.y].height = realContentSize.height;
        }
        if(tbleFrame->maxCellSize[posInTable.x].width < realContentSize.width){
           tbleFrame->maxCellSize[posInTable.x].width = realContentSize.width;
        }

        tbleFrame->allContent[posInTable.y][posInTable.x].color = color;
        tbleFrame->allContent[posInTable.y][posInTable.x].size = contentSize;
        tbleFrame->allContent[posInTable.y][posInTable.x].content = 
            calloc(contentSize, sizeof(char));
        for(int i=0; i < contentSize; i++){
            tbleFrame->allContent[posInTable.y][posInTable.x].content[i] = content[i];
        }
    }
}

void createCellTableLabel(TableFrame * tbleFrame, coordinate posInTable, labelPos labPos,  
                     char * content, int contentSize, int color, 
                     verticalPosition vertPos, horizontalPosition horPos,
                     size spacing){

    char testHeight = labPos == leftLabelPos || labPos == rightLabelPos;
    char testWidth = labPos == topLabelPos || labPos == bottomLabelPos;

    if(((testWidth && posInTable.x < tbleFrame->tableSize.width) || 
       (!testWidth && posInTable.x <= 1)) && posInTable.x >= 0 &&
       ((testHeight && posInTable.y < tbleFrame->tableSize.height) || 
       (!testHeight && posInTable.y <= 1)) && posInTable.y >= 0){

        /* Créer le label si n'existe pas encore*/
        if(tbleFrame->allLabels[labPos].table != tbleFrame){
            tbleFrame->allLabels[labPos].table = tbleFrame;
            tbleFrame->allLabels[labPos].isLabelLocked = False;
            if(testWidth){
                tbleFrame->allLabels[labPos].labelSize.width = 
                    tbleFrame->tableSize.width;
            } else {
                tbleFrame->allLabels[labPos].labelSize.width =
                    tbleFrame->tableSize.height;
            }

            tbleFrame->allLabels[labPos].allContent = 
                calloc(tbleFrame->allLabels[labPos].labelSize.width, 
                sizeof(cellContent));
            tbleFrame->allLabels[labPos].labelCellSize =
                calloc(tbleFrame->allLabels[labPos].labelSize.width, 
                sizeof(size));
            tbleFrame->allLabels[labPos].labelCellSpacingSize = 
                calloc(tbleFrame->allLabels[labPos].labelSize.width, 
                sizeof(size));
            tbleFrame->allLabels[labPos].labelCellVerticalPosition =
                calloc(tbleFrame->allLabels[labPos].labelSize.width, 
                sizeof(verticalPosition));
            tbleFrame->allLabels[labPos].labelCellHorizontalPosition =
                calloc(tbleFrame->allLabels[labPos].labelSize.width, 
                sizeof(horizontalPosition));
        }

        copySize(&spacing, 
            &tbleFrame->allLabels[labPos].labelCellSpacingSize[posInTable.x]);
        tbleFrame->allLabels[labPos].labelCellHorizontalPosition[posInTable.x] = 
            horPos;
        tbleFrame->allLabels[labPos].labelCellVerticalPosition[posInTable.x] = 
            vertPos;

        
        size realContentSize = getRealSize(content, contentSize);
        realContentSize.height += 
            tbleFrame->allLabels[labPos].labelCellSpacingSize[posInTable.x].height;
        if(tbleFrame->allLabels[labPos].labelCellVerticalPosition[posInTable.x] == centerVert){
            realContentSize.height += 
                tbleFrame->allLabels[labPos].labelCellSpacingSize[posInTable.x].height;
        }
        realContentSize.width += 
            tbleFrame->allLabels[labPos].labelCellSpacingSize[posInTable.x].width;
        if(tbleFrame->allLabels[labPos].labelCellHorizontalPosition[posInTable.x] == centerHori){
            realContentSize.width += 
                tbleFrame->allLabels[labPos].labelCellSpacingSize[posInTable.x].width;
        }
        copySize(&realContentSize,
                 &tbleFrame->allLabels[labPos].labelCellSize[posInTable.x]);

        if(testWidth){
            if(tbleFrame->maxCellSize[posInTable.x].width < realContentSize.width){
                tbleFrame->maxCellSize[posInTable.x].width = realContentSize.width;
            }
            if(tbleFrame->allLabels[labPos].maxSize < realContentSize.height){
                tbleFrame->allLabels[labPos].maxSize = realContentSize.height;
            }
        } else {
            if(tbleFrame->maxCellSize[posInTable.x].height < realContentSize.height){
                tbleFrame->maxCellSize[posInTable.x].height = realContentSize.height;
            }
            if(tbleFrame->allLabels[labPos].maxSize < realContentSize.width){
                tbleFrame->allLabels[labPos].maxSize = realContentSize.width;
            }
        }

        tbleFrame->allLabels[labPos].allContent[posInTable.x].color = color;
        tbleFrame->allLabels[labPos].allContent[posInTable.x].size = contentSize;
        tbleFrame->allLabels[labPos].allContent[posInTable.x].content = 
            calloc(contentSize, sizeof(char));
        for(int i=0; i < contentSize; i++){
            tbleFrame->allLabels[labPos].allContent[posInTable.x].content[i] = content[i];
        }
    }

}

void lockTableLabel(TableFrame * tbleFrame, labelPos labPos){
    tbleFrame->allLabels[labPos].isLabelLocked = True;
}

void unlockTableLabel(TableFrame * tbleFrame, labelPos labPos){
    tbleFrame->allLabels[labPos].isLabelLocked = False;
}

void printTableOutline(WINDOW * winTable, int totHeight, int totWidth, int * cumulHeight,
                int cumulHeightSize, int * cumulWidth, int cumulWidthSize, 
                int labSide){

    /*Met les lignes horizontales et les croisements entre les cases*/
    for(int i=0; i < cumulHeightSize; i++){
        for(int j=0; j < totWidth; j++){

            if((cumulHeight[i] != 0 && j != 0) || (cumulHeight[i] != totHeight-1
               && j != 0) || (cumulHeight[i] != 0 && j != totWidth-1) || (
               cumulHeight[i] != totHeight-1 && j != totWidth-1)){

                boolean isCrossed = False;
                for(int k=0; k < cumulWidthSize; k++){
                    if(j == cumulWidth[k]){
                        isCrossed = True;
                    }
                }

                if(isCrossed){
                    if((cumulHeight[i] != 0 && cumulHeight[i] != totHeight-1 
                       && j != 0 && j != totWidth-1) || (labSide == topLabelPos &&
                       cumulHeight[i] == totHeight-1) || (labSide == bottomLabelPos
                       && cumulHeight[i] == 0) || (labSide == leftLabelPos && 
                       j == totWidth-1) || (labSide == rightLabelPos && j == 0)){

                        mvwaddch(winTable, cumulHeight[i], j, ACS_PLUS);
                    } else if(cumulHeight[i] == 0 && j != 0 && j != totWidth-1){

                        mvwaddch(winTable, cumulHeight[i], j, ACS_TTEE);
                    } else if(cumulHeight[i] == totHeight-1 && j != 0 
                              && j != totWidth-1){

                        mvwaddch(winTable, cumulHeight[i], j, ACS_BTEE);
                    } else if(cumulHeight[i] != 0 && cumulHeight[i] != totHeight-1 
                              && j == 0){

                        mvwaddch(winTable, cumulHeight[i], j, ACS_LTEE);
                    } else if(cumulHeight[i] != 0 && cumulHeight[i] != totHeight-1 
                              && j == totWidth-1){
                        mvwaddch(winTable, cumulHeight[i], j, ACS_RTEE);
                    }
                } else {
                    mvwaddch(winTable, cumulHeight[i], j, ACS_HLINE);
                }
            }
        }
    }

    /*Met les lignes verticales*/
    for(int i=0; i < cumulWidthSize; i++){
        for(int j=0; j < totHeight; j++){
            boolean isCrossed = False;
            for(int k=0; k < cumulHeightSize; k++){
                if(j == cumulHeight[k]){
                    isCrossed = True;
                }
            }

            if(!isCrossed){
                mvwaddch(winTable, j, cumulWidth[i], ACS_VLINE);
            }
        }
    }

    /*Met les coins dans le tableau*/
    mvwaddch(winTable, 0, 0, ACS_ULCORNER);
    mvwaddch(winTable, 0, totWidth-1, ACS_URCORNER);
    mvwaddch(winTable, totHeight-1, 0, ACS_LLCORNER);
    mvwaddch(winTable, totHeight-1, totWidth-1, ACS_LRCORNER);
    fprintf(stderr, "Size win: %d %d\n", totHeight-1, totWidth-1);
}

void printCellTable(WINDOW * table, cellContent content, coordinate cellStart, 
                    size contentSize, size cellSize, verticalPosition vPos, 
                    horizontalPosition hPos, size spacing){
    (void) spacing;

    int vNewPos = allVerticalPositionFunc[vPos](contentSize.height, cellSize.height);

    int * contWidth = calloc(contentSize.height, sizeof(int));
    if(contentSize.height == 1){
        contWidth[0] = contentSize.width;
    } else {
        int widthPos = 0;
        for(int i=0; i < content.size - 1; i++){
            if(content.content[i] == '\n'){
                widthPos++;
            } else if(content.content[i] != '\0'){
                contWidth[widthPos]++;
            }
        }
    }

    int * hNewPos = calloc(contentSize.height, sizeof(int));

    for(int i=0; i < contentSize.height; i++){
        hNewPos[i] = allHorizontalPositionFunc[hPos](contWidth[i], cellSize.width);
    }

    fprintf(stderr, "\n\nPrinting %s, at (%d,%d) with a size of (%d,%d):\n", content.content, cellStart.x, cellStart.y, cellSize.width, cellSize.height);

    int curContentPos = 0;
    wattr_set(table, 0, content.color, NULL);
    for(int height=0; height < cellSize.height; height++){
        for(int width=0; width < cellSize.width ; width++){
            if(height >= vNewPos && height < vNewPos + contentSize.height && 
               width >= hNewPos[height - vNewPos] && 
               width < hNewPos[height - vNewPos] + contWidth[height - vNewPos]){

                mvwprintw(table, height + cellStart.y, width + cellStart.x, "%c", 
                          content.content[curContentPos]);
                curContentPos++;
                fprintf(stderr, "Height: %d, Width: %d, vNewPos: %d, content Height: %d, hNewPos: %d, contWidth: %d, char: %c\n",
                        height, width, vNewPos, contentSize.height, hNewPos[height-vNewPos], contWidth[height-vNewPos], content.content[curContentPos]);
            } else {
                mvwprintw(table, height + cellStart.y, width + cellStart.x, " ");
            }
        }
    }
    wattr_set(table, 0, allColors[Black][White], NULL);

    free(contWidth);
    free(hNewPos);
}

void genTableFrame(TableFrame * tbleFrame){
    
    int tableHeight = 1 + tbleFrame->tableSize.height;
    int * cumulHeight = calloc(tbleFrame->tableSize.height + 1, sizeof(int));
    int tableWidth = 1 + tbleFrame->tableSize.width;
    int * cumulWidth = calloc(tbleFrame->tableSize.width + 1, sizeof(int));

    /*Calcul la hauteur du tableau finale*/
    for(int i=0; i < tbleFrame->tableSize.height; i++){
        cumulHeight[i] = tableHeight - 1 - tbleFrame->tableSize.height + i;
        tableHeight += tbleFrame->maxCellSize[i].height;
    }
    tbleFrame->realTableSize.height = tableHeight;
    cumulHeight[tbleFrame->tableSize.height] = tableHeight - 1;

    /*Calcul la largeur du tableau finale*/
    for(int i=0; i < tbleFrame->tableSize.width; i++){
        cumulWidth[i] = tableWidth - 1 - tbleFrame->tableSize.width + i;
        tableWidth += tbleFrame->maxCellSize[i].width;
    }
    cumulWidth[tbleFrame->tableSize.width] = tableWidth - 1;
    tbleFrame->realTableSize.width = tableWidth;

    tbleFrame->table = newpad(tableHeight, tableWidth);
    for(int i=0; i < 4; i++){
        if(tbleFrame->allLabels[i].table == tbleFrame){

            int labHeight = tbleFrame->allLabels[i].maxSize + 2;
            int * labCumulHeight = calloc(2, sizeof(int));
            labCumulHeight[1] = tbleFrame->allLabels[i].maxSize + 2;
            int labCumulHeightSize = 2;

            int labWidth = tableWidth;
            int * labCumulWidth = cumulWidth;
            int labCumulWidthSize = tbleFrame->tableSize.width + 1;

            if(i == leftLabelPos || i == rightLabelPos){
                labWidth = labHeight;
                labCumulWidth = labCumulHeight;
                labCumulWidthSize = labCumulHeightSize;

                labHeight = tableHeight;
                labCumulHeight = cumulHeight;
                labCumulHeightSize = tbleFrame->tableSize.height + 1;
            }

            tbleFrame->allLabels[i].label = newpad(labHeight, labWidth);
            printTableOutline(tbleFrame->allLabels[i].label, labHeight, labWidth, 
                       labCumulHeight, labCumulHeightSize, labCumulWidth, 
                       labCumulWidthSize, i);

            if(i == leftLabelPos || i == rightLabelPos){
                free(labCumulWidth);
            } else {
                free(labCumulHeight);
            }
        }
    }

    /*Affiche les contours de tableau*/
    printTableOutline(tbleFrame->table, tableHeight, tableWidth, cumulHeight, 
               tbleFrame->tableSize.height + 1, cumulWidth, 
               tbleFrame->tableSize.width + 1, -1);

    /*Affiche le contenu du tableau*/
    for(int height=0; height < tbleFrame->tableSize.height; height++){
        for(int width=0; width < tbleFrame->tableSize.width; width++){
            size realContentSize;
            copySize(&tbleFrame->tableCellSize[height][width], &realContentSize);
            realContentSize.height -= tbleFrame->tableCellSpacingSize[height][width].height;
            realContentSize.width -= tbleFrame->tableCellSpacingSize[height][width].width;
            if(tbleFrame->tableCellHorizontalPosition[height][width] == centerHori){
                realContentSize.width -= tbleFrame->tableCellSpacingSize[height][width].width;
            }
            if(tbleFrame->tableCellVerticalPosition[height][width] == centerVert){
                realContentSize.height -= tbleFrame->tableCellSpacingSize[height][width].height;
            }

            printCellTable(tbleFrame->table, tbleFrame->allContent[height][width], 
                           createCoordinate(cumulWidth[width]+1, cumulHeight[height]+1), 
                           realContentSize, 
                           createSize(cumulHeight[height+1] - cumulHeight[height] - 1, 
                           cumulWidth[width+1] - cumulWidth[width] -1), 
                           tbleFrame->tableCellVerticalPosition[height][width], 
                           tbleFrame->tableCellHorizontalPosition[height][width], 
                           tbleFrame->tableCellSpacingSize[height][width]);
        }
    }

    updateSizeTableFrame((void *) tbleFrame);
    refresh();
    free(cumulHeight);
    free(cumulWidth);
}

void freeTableFrame(void ** tableFrame){
    TableFrame * tbleFrame = (TableFrame *) *tableFrame;

    for(int i=0; i < tbleFrame->tableSize.height; i++){
        for(int j=0; j < tbleFrame->tableSize.width; j++){
            free(tbleFrame->allContent[i][j].content);
        }

        free(tbleFrame->allContent[i]);
        free(tbleFrame->tableCellHorizontalPosition[i]);
        free(tbleFrame->tableCellVerticalPosition[i]);
        free(tbleFrame->tableCellSpacingSize[i]);
        free(tbleFrame->tableCellSize[i]);
    }

    for(int i=0; i < 4; i++){
        if(tbleFrame->allLabels[i].table == tbleFrame){
            for(int j=0; j  < tbleFrame->allLabels[i].labelSize.width; j++){
                free(tbleFrame->allLabels[i].allContent[j].content);
            }

            delwin(tbleFrame->allLabels[i].label);
            free(tbleFrame->allLabels[i].allContent);
            free(tbleFrame->allLabels[i].labelCellHorizontalPosition);
            free(tbleFrame->allLabels[i].labelCellVerticalPosition);
            free(tbleFrame->allLabels[i].labelCellSpacingSize);
            free(tbleFrame->allLabels[i].labelCellSize);
        }
    }

    delwin(tbleFrame->table);
    free(tbleFrame->allContent);
    free(tbleFrame->allLabels);
    free(tbleFrame->maxCellSize);
    free(tbleFrame->tableCellSize);
    free(tbleFrame->tableCellSpacingSize);
    free(tbleFrame->tableCellHorizontalPosition);
    free(tbleFrame->tableCellVerticalPosition);
    *tableFrame = NULL;
}

Frame * getTableFrameFrame(void * tableFrame){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;
    return  tbleFrame->f;
}

void printTableFrame(void * tableFrame){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;

    if(tbleFrame->f->isFrameVisible){

        //Par manque de temps nous n'allons pas nous occuper des labels
        //car nous n'allons pas réellement en avoir besoin dans cette
        //simulation
        
        wrefresh(tbleFrame->table);
        prefresh(tbleFrame->table, tbleFrame->posInTable.y, tbleFrame->posInTable.x, 
                 tbleFrame->f->framePos.y, tbleFrame->f->framePos.x, 
                 tbleFrame->f->framePos.y + tbleFrame->sizeTableShown.height, 
                 tbleFrame->f->framePos.x + tbleFrame->sizeTableShown.width);
        

        /*fprintf(stderr, "OI(%d,%d), O(%d, %d), size: (%d,%d)\n",tbleFrame->posInTable.x, tbleFrame->posInTable.y,
                tbleFrame->f->framePos.x, tbleFrame->f->framePos.y, tbleFrame->f->framePos.x + tbleFrame->sizeTableShown.width,
                tbleFrame->f->framePos.y + tbleFrame->sizeTableShown.height);*/
        //wrefresh(tbleFrame->table);

        printFrame((void *) tbleFrame->f);
        //doupdate();
    }
}

void updateSizeTableFrame(void * tableFrame){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;

    updateSizeFrame((void *) tbleFrame->f);

    if(tbleFrame->f->frameSize.width >= tbleFrame->realTableSize.width){
        tbleFrame->sizeTableShown.width = tbleFrame->realTableSize.width;
        tbleFrame->f->canMoveRight = False;
    } else {
        tbleFrame->sizeTableShown.width = tbleFrame->f->frameSize.width;
        tbleFrame->f->canMoveRight = True;
    }

    if(tbleFrame->posInTable.x + tbleFrame->sizeTableShown.width >= 
       tbleFrame->realTableSize.width){

        tbleFrame->posInTable.x -=  tbleFrame->posInTable.x + 
            tbleFrame->sizeTableShown.width - tbleFrame->realTableSize.width;
        tbleFrame->f->canMoveLeft = False;
    } else if(tbleFrame->posInTable.x != 0){
        tbleFrame->f->canMoveLeft = True;
    }

    tbleFrame->f->frameSize.width = tbleFrame->sizeTableShown.width;
    if(tbleFrame->f->frameSize.height >= tbleFrame->realTableSize.height){
        tbleFrame->sizeTableShown.height = tbleFrame->realTableSize.height;
        tbleFrame->f->canMoveDown = False;
    } else {
        tbleFrame->sizeTableShown.height = tbleFrame->f->frameSize.height;
        tbleFrame->f->canMoveDown = True;
    }

    if(tbleFrame->posInTable.y + tbleFrame->sizeTableShown.height >= 
       tbleFrame->realTableSize.height){

        tbleFrame->posInTable.y -=  tbleFrame->posInTable.y + 
            tbleFrame->sizeTableShown.height - tbleFrame->realTableSize.height;
        tbleFrame->f->canMoveUp = False;
    } else if(tbleFrame->posInTable.y != 0){
        tbleFrame->f->canMoveUp = True;
    }
    tbleFrame->f->frameSize.height = tbleFrame->sizeTableShown.height;
}

boolean isTableFrameSelected(void * tableFrame, int key){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;

    if(isFrameSelected((void *) tbleFrame->f, key)){
        printTableFrame((void *) tbleFrame);
        return True;
    }

    return False;
}

void unselectTableFrame(void * tableFrame){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;

    unselectFrame((void *) tbleFrame->f);
    printTableFrame((void *) tbleFrame);
}

void inputKeyTableFrame(void * tableFrame, int key){
    TableFrame * tbleFrame = (TableFrame *) tableFrame;

    if(tbleFrame->f->isFrameVisible && tbleFrame->f->isFrameSelected){
        if(tbleFrame->posInTable.x > 0 && key == KEY_LEFT){

            tbleFrame->posInTable.x--;
            if(tbleFrame->posInTable.x == 0){
                tbleFrame->f->canMoveLeft = False;
            }
            if(tbleFrame->posInTable.x + tbleFrame->sizeTableShown.width 
               < tbleFrame->realTableSize.width){
                tbleFrame->f->canMoveRight = True;
            }
            printTableFrame((void *) tbleFrame);
        } else if(tbleFrame->posInTable.y > 0 && key == KEY_UP){

            tbleFrame->posInTable.y--;
            if(tbleFrame->posInTable.y == 0){
                tbleFrame->f->canMoveUp = False;
            }
            if(tbleFrame->posInTable.y + tbleFrame->sizeTableShown.height 
               < tbleFrame->realTableSize.height){
                tbleFrame->f->canMoveDown = True;
            }
            printTableFrame((void *) tbleFrame);
        } else if(tbleFrame->posInTable.x + tbleFrame->sizeTableShown.width <
                  tbleFrame->realTableSize.width && key == KEY_RIGHT){

            tbleFrame->posInTable.x++;
            if(tbleFrame->posInTable.x + tbleFrame->sizeTableShown.width
               == tbleFrame->realTableSize.width){
                tbleFrame->f->canMoveRight = False;
            }
            if(tbleFrame->posInTable.x > 0){
                tbleFrame->f->canMoveLeft = True;
            }
            printTableFrame((void *) tbleFrame);
        } else if(tbleFrame->posInTable.y + tbleFrame->sizeTableShown.height <
                  tbleFrame->realTableSize.height && key == KEY_DOWN){

            tbleFrame->posInTable.y++;
            if(tbleFrame->posInTable.y + tbleFrame->sizeTableShown.height
               == tbleFrame->realTableSize.height){
                tbleFrame->f->canMoveDown = False;
            }
            if(tbleFrame->posInTable.y > 0){
                tbleFrame->f->canMoveUp = True;
            }
            printTableFrame((void *) tbleFrame);
        }


    }
}

