#ifndef __FRAME_H__
#define __FRAME_H__

#include <ncurses.h>

#include "../DefaultLib/position.h"
#include "../DefaultLib/boolean.h"
#include "Screen.h"

typedef enum verticalPosition{
    centerVert,
    topVert,
    bottomVert,
} verticalPosition;

typedef int (*verticalPositionFuncType) (int elemSize, int envSize);
extern verticalPositionFuncType allVerticalPositionFunc[];

typedef enum horizontalPosition{
    centerHori,
    leftHori,
    rightHori,
} horizontalPosition;

typedef int (*horizontalPositionFuncType) (int elemSize, int envSize);
extern horizontalPositionFuncType allHorizontalPositionFunc[];


typedef struct Frame{
    Screen ** scr;
    int frameId;
    coordinate frameIdPosInFrame;
    int frameColor;
    int arrowSpacing;

    boolean canMoveUp: 1;
    boolean canMoveDown: 1;
    boolean canMoveRight: 1;
    boolean canMoveLeft: 1;

    boolean isFrameSelected:1;
    boolean isFrameVisible:1;

    coordinate framePos;
    size frameSize;

    coordinatePercent framePosPercent;
    sizePercent frameSizePercent;
} Frame;


Frame * createFrame(int frameId, coordinatePercent framePosInPercent, 
                    sizePercent frameSizeInPercent);

void freeFrame(void ** frame);

void printFrame(void * frame);

void updateSizeFrame(void * frame);

boolean isFrameSelected(void * frame, int key);

void unselectFrame(void * frame);

//void inputKeyFrame(void * frame, int key);


int getSpaceCenterVertPos(int elemSize, int envSize);

int getSpaceTopVertPos(int elemSize, int envSize);

int getSpaceBottomVertPos(int elemSize, int envSize);

int getSpaceCenterHoriPos(int elemSize, int envSize);

int getSpaceLeftHoriPos(int elemSize, int envSize);

int getSpaceRightHoriPos(int elemSize, int envSize);

#endif