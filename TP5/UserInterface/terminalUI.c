#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <ncurses.h>

#include "terminalUI.h"

void initTerminalUI(){

    // Active l'utilisation de charactère en UTF-8
    setlocale(LC_ALL, "");

    // Initialise ncurses
    WINDOW * win = initscr();

    // clear le terminal
    clear();

    // désactive l'affichage des touches claviers
	noecho();

    // désactive la récupération des entrées clavier uniquement après un retour à la ligne
	cbreak();

    // efface la position du curseur
    curs_set(0);

    // active la récupération des touches qui ne sont pas des lettres
    keypad(win, TRUE);

    // initialise les couleurs si le terminal les supporte
    if(has_colors()){
        initAllColors();
    }else{
        allColors = NULL;
    }
}

void freeTerminalUI(){

    // free les couleurs si le terminal les supporte
    if(has_colors()){
        freeAllColors();
    }

    // active la position du curseur
    curs_set(1);

    // réactive l'affichage des touches claviers
    echo();

    // réactive la récupération des entrées clavier uniquement après un retour à la ligne
    nocbreak();

    // libère ncurses
    endwin();
}