#ifndef __TABLE_FRAME_H__
#define __TABLE_FRAME_H__

#include <ncurses.h>

#include "../DefaultLib/boolean.h"
#include "color.h"
#include "Frame.h"

typedef enum labelPos{
    topLabelPos,
    bottomLabelPos,
    leftLabelPos,
    rightLabelPos
} labelPos;

typedef struct TableFrame TableFrame;

typedef struct cellContent{
    char * content;
    int size;
    int color;
} cellContent;

typedef struct TableLabel{
    TableFrame * table;
    boolean isLabelLocked: 1;

    cellContent * allContent;
    size * labelCellSize;
    int maxSize;
    size * labelCellSpacingSize;
    verticalPosition * labelCellVerticalPosition;
    horizontalPosition * labelCellHorizontalPosition;

    WINDOW * label;
    size labelSize;

} TableLabel;

typedef struct TableFrame{
    Frame * f;

    size tableSize;
    cellContent ** allContent;
    size ** tableCellSize;
    size ** tableCellSpacingSize;
    verticalPosition ** tableCellVerticalPosition;
    horizontalPosition ** tableCellHorizontalPosition;
    size * maxCellSize;

    TableLabel * allLabels;

    WINDOW * table;
    size realTableSize;
    size tableWindowSize;
    coordinate posInTable;
    size sizeTableShown;
} TableFrame;



TableFrame * createTableFrame(int frameId, coordinatePercent framePosInPercent, 
                              sizePercent frameSizeInPercent, size tableSize);

void createCellTableFrame(TableFrame * tbleFrame, coordinate posInTable, 
                          char * content, int contentSize, int color, 
                          verticalPosition vertPos, horizontalPosition horPos,
                          size spacing);

void createCellTableLabel(TableFrame * tbleFrame, coordinate posInTable, labelPos labPos,  
                     char * content, int contentSize, int color, 
                     verticalPosition vertPos, horizontalPosition horPos,
                     size spacing);

void lockTableLabel(TableFrame * tbleFrame, labelPos labPos);

void unlockTableLabel(TableFrame * tbleFrame, labelPos labPos);

void genTableFrame(TableFrame * tbleFrame);

void freeTableFrame(void ** tableFrame);

Frame * getTableFrameFrame(void * tableFrame);

void printTableFrame(void * tableFrame);

void updateSizeTableFrame(void * tableFrame);

boolean isTableFrameSelected(void * tableFrame, int key);

void unselectTableFrame(void * tableFrame);

void inputKeyTableFrame(void * tableFrame, int key);

void printTableCellContent(TableFrame * tbleFrame, coordinate cellPos, char * content, 
                           size contentSize, int couleur, verticalPosition vertPos, 
                           horizontalPosition horPos);

#endif