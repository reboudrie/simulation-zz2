#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "Frame.h"
#include "ContentFrame.h"
#include "TableFrame.h"

// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de free chaque type de Frame
typedef void (*freeFrameTypeFunc)(void ** frame);
freeFrameTypeFunc allFreeFrameFunc[] = {freeContentFrame};


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de print chaque type de Frame
typedef void (*printFrameTypeFunc)(void * frame);
printFrameTypeFunc allPrintFrameFunc[] = {printContentFrame};


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de récupérer l'élément Frame de chaque type de Frame
typedef Frame * (*getFrameTypeFrameFunc)(void * frame);
getFrameTypeFrameFunc allGetFrameFrameFunc[] = {getContentFrameFrame};


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de mettre à jour la taille de chaque type de Frame
typedef void (*updateSizeFrameTypeFunc)(void * frame);
updateSizeFrameTypeFunc allUpdateSizeFrameFunc[] = {updateSizeContentFrame};


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de vérifier et de sélectionner la Frame en fonction
// de l'entrée clavier mise en entrée et retourne Vrai si la Frame a été
// sélectionné
typedef boolean (*isFrameSelectedTypeFunc)(void * frame, int key);
isFrameSelectedTypeFunc allIsFrameSelectedFunc[] = {isContentFrameSelected};


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent de déselectionner la Frame de chaque type de Frame
typedef void (*unselectFrameTypeFunc)(void * frame);
unselectFrameTypeFunc allUnselectFrameFunc[] = {};
    /*unselectContentFrame,
    unselectTableFrame};*/


// Défini un tableau global qui contient des pointeurs sur les fonctions
// qui permettent d'effectuer les changement sur la Frame en focntion
// de l'entrée clavier passée en entrée pour chaque type de Frame
typedef void (*inputKeyFrameTypeFunc)(void * frame, int key);
inputKeyFrameTypeFunc allInputKeyFrameFunc[] = {inputKeyContentFrame};



Screen * initScreen(){
    // Alloue la mémoire pour le Screen
    Screen * scr = calloc(1, sizeof(Screen));

    // Rempli les valeurs de Screen et alloue la mémoire pour les tableaux
    scr->nbFrames = 0;
    scr->sizeAllFrames = DEFAULT_SCREEN_SIZE;
    scr->allFrames = calloc(DEFAULT_SCREEN_SIZE, sizeof(void *));
    scr->allFramesType = calloc(DEFAULT_SCREEN_SIZE, sizeof(frameType));

    return scr;
}


void freeScreen(Screen ** scr, boolean freeAllFrames){

    // Appel la fonction pour free chaque Frame si le booléen en entrée est Vrai
    if(freeAllFrames){
        for(int i=0; i<(*scr)->nbFrames; i++){
            allFreeFrameFunc[(*scr)->allFramesType[i]](&(*scr)->allFrames[i]);
        }
    }

    // Free les tableaux dans Screen et Screen et les met à NULL
    free((*scr)->allFrames);
    (*scr)->allFrames = NULL;
    free((*scr)->allFramesType);
    (*scr)->allFramesType = NULL;
    free((*scr));
    (*scr) = NULL;
}


void addFrameToScreen(Screen ** scr, void * frame, frameType type){

    // Vérifie que le tableau de toutes les Frame peut acceuillir une nouvelle
    // et l'agrandi si le tableau est trop petit
    if((*scr)->sizeAllFrames == (*scr)->nbFrames){
        
        // Augmenter la taille des tableaux en multipliant l'ancienne par 2
        (*scr)->sizeAllFrames *= 2;
        void ** newAllFrames = calloc((*scr)->sizeAllFrames, sizeof(void *));
        frameType * newAllFramesType = calloc((*scr)->sizeAllFrames, sizeof(frameType));

        // Remet les anciennes valeurs présentent dans les tableaux dans
        // les nouveaux
        for(int i=0; i<(*scr)->nbFrames; i++){
            newAllFrames[i] = (*scr)->allFrames[i];
            newAllFramesType[i] = (*scr)->allFramesType[i];
        }

        // Libère les anciens tableaux et met dans Screen les nouveaux
        free((*scr)->allFrames);
        free((*scr)->allFramesType);
        (*scr)->allFrames = newAllFrames;
        (*scr)->allFramesType = newAllFramesType;
    }

    // Met le screen dans la Frame que l'on veut ajouter au Screen
    Frame * frameTypeFrame = allGetFrameFrameFunc[type](frame);
    frameTypeFrame->scr = scr;

    // Ajoute les valeurs de la Frame que l'on veut ajouter dans le tableau
    // de toutes les Frame et le tableau du type de tous les types de Frame
    (*scr)->allFrames[(*scr)->nbFrames] = frame;
    (*scr)->allFramesType[(*scr)->nbFrames] = type;
    (*scr)->nbFrames++;
}


void delFrameFromScreen(Screen * scr, void * frame, boolean doFreeFrame){

    // Cherche la position de la Frame dans le tableau de toutes les Frames
    // dans Screen
    int pos = 0;
    for(pos = 0; pos < scr->nbFrames && scr->allFrames[pos] != frame; pos++);

    if(pos < scr->nbFrames){

        Frame * frameTypeFrame = allGetFrameFrameFunc[scr->allFramesType[pos]](scr->allFrames[pos]);
        frameTypeFrame->scr = NULL;

        // Free la Frame si le booléen en entrée est vrai
        if(doFreeFrame){
            allFreeFrameFunc[scr->allFramesType[pos]](&scr->allFrames[pos]);
        }

        // Suprime la Frame du tableau et diminue le nombre de Frame de 
        // celui-ci de 1
        scr->allFrames[pos] = NULL;
        scr->nbFrames--;

        // Déplace toutes les Frame qui se trouvaient arpès la Frame suprimée
        for(int i=pos; i<scr->nbFrames; i++){
            scr->allFrames[i] = scr->allFrames[i+1];
            scr->allFramesType[i] = scr->allFramesType[i+1];
        }
        scr->allFrames[scr->nbFrames] = NULL;
    }

}


void printScreen(Screen * scr){

    // Parcour toutes les Frame de Screen
    for(int i=0; i<scr->nbFrames; i++){

        // Récupère la structure Frame de l'élément courant
        Frame * frameTypeFrame = allGetFrameFrameFunc[scr->allFramesType[i]](scr->allFrames[i]);
        
        // Appel la fonction pour print la Frame si la frame est visible
        if(frameTypeFrame->isFrameVisible){
            allPrintFrameFunc[scr->allFramesType[i]](scr->allFrames[i]);
        }
    }
}


void screenCheckAndUpdate(Screen * scr, int key){

    // Vérifie qu'une touche à moins été appuyé
    if(key != ERR){

        // Vérifie si le terminal n'a pas été redimensionné et le met à
        // jour si c'est le cas
        if(key == KEY_RESIZE){

            // Efface l'intégralité de ce qui se trouve dans le terminal
            werase(stdscr);

            for(int i=0; i<scr->nbFrames; i++){

                // Appel la fonction pour redimensionner la Frame
                allUpdateSizeFrameFunc[scr->allFramesType[i]](scr->allFrames[i]);
            }

            // Réaffiche l'entièreté du terminal avec les nouvelles dimensions
            printScreen(scr);

        }else{
            int i=0;
            for(i=0; i<scr->nbFrames; i++){

                // Regarde si la Frame est sélectionné depuis l'entrée clavier
                if(allIsFrameSelectedFunc[scr->allFramesType[i]](scr->allFrames[i], key)){

                    for(int j=0; j<scr->nbFrames; j++){

                        Frame * frameTypeFrame = 
                        allGetFrameFrameFunc[scr->allFramesType[j]](scr->allFrames[j]);

                        // Regarde si la Frame est sélectionné et si c'est le cas la déselectionne
                        if(j != i && frameTypeFrame->isFrameSelected){
                            allUnselectFrameFunc[scr->allFramesType[j]](scr->allFrames[j]);
                        }
                    }

                    break;
                }
            }

            // Vérifie qu'aucun changement de sélection de Frame a été effectué car si c'est
            // le cas nous savons déjà que le caractère dans key ne nous est pas utile
            if(i == scr->nbFrames){
                for(i=0; i<scr->nbFrames; i++){

                    Frame * frameTypeFrame = 
                    allGetFrameFrameFunc[scr->allFramesType[i]](scr->allFrames[i]);

                    // Cherche la frame actuellement sélectionné et appel la fonction pour
                    // vérifier si l'entrée clavier est utilisable
                    if(frameTypeFrame->isFrameSelected){
                        allInputKeyFrameFunc[scr->allFramesType[i]](scr->allFrames[i], key);
                        break;
                    }
                }
            }
        }
    }
}