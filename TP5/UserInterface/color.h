#ifndef __COLOR_H__
#define __COLOR_H__

typedef enum Color{
    Black = 0,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,

    //Colors pour la simulation
    C_GrassDay,         //= 34
    C_GrassNight,       // = 29
    C_DirtDay,          //= 136
    C_DirtNight,        //= 59
    C_SheepDay,         //= 254
    C_SheepNight,       //= 104
    C_WolfDay,          //= 247
    C_WolfNight,        //= 60

    NB_COLORS
} Color;

/*Store all the color pairs, the line number is the background and the
column number is the foreground*/
extern int ** allColors;

void initAllColors();

void freeAllColors();

int getReverseColor(int baseColor);

#endif