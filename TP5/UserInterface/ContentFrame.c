#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "ContentFrame.h"
#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "Frame.h"
#include "color.h"


ContentFrame * createContentFrame(int frameId, coordinatePercent framePosInPercent, 
                                  sizePercent frameSizeInPercent, size contentSize, 
                                  size spacingWithBox, verticalPosition verticalPostionInTheBox, 
                                  horizontalPosition horizontalPositionInTheBox){

    ContentFrame * contFrame = calloc(1, sizeof(ContentFrame));

    contFrame->f = createFrame(frameId, framePosInPercent, frameSizeInPercent);
    contFrame->f->frameIdPosInFrame.x = 2;
    contFrame->f->isFrameVisible = True;
    copySize(&contentSize, &contFrame->contentSize);
    copySize(&spacingWithBox, &contFrame->spacingWithBox);
    contFrame->spacingWithBox.height ++;
    contFrame->spacingWithBox.width ++;
    contFrame->verticalPostionInTheBox = verticalPostionInTheBox;
    contFrame->horizontalPositionInTheBox = horizontalPositionInTheBox;
    copySize(&contFrame->f->frameSize, &contFrame->winBoxSize);
    contFrame->showPosInContent.x = 0;
    contFrame->showPosInContent.y = 0;
    contFrame->posInBox = createCoordinate(0, 0);

    contFrame->winBox = newwin(contFrame->winBoxSize.height, contFrame->winBoxSize.width,
        contFrame->f->framePos.y, contFrame->f->framePos.x);
    contFrame->content = newpad(contFrame->contentSize.height, contFrame->contentSize.width);

    updateSizeContentFrame((void *) contFrame);

    return contFrame;
}

void freeContentFrame(void ** contentFrame){
    ContentFrame ** contFrame = (ContentFrame **) contentFrame;

    freeFrame((void **) &(*contFrame)->f);
    delwin((*contFrame)->content);
    delwin((*contFrame)->winBox);

    free(*contFrame);
    *contFrame = NULL;
}

void printContentFrame(void * contentFrame){
    ContentFrame * contFrame = (ContentFrame *) contentFrame;

    if(contFrame->f->isFrameVisible){
        printFrame((void *) contFrame->f);
        
        prefresh(contFrame->content, contFrame->showPosInContent.y, contFrame->showPosInContent.x,
            contFrame->f->framePos.y + contFrame->spacingWithBox.height + contFrame->posInBox.y, 
            contFrame->f->framePos.x + contFrame->spacingWithBox.width + contFrame->posInBox.x, 
            contFrame->f->framePos.y + contFrame->spacingWithBox.height + contFrame->posInBox.y + contFrame->contentShowSize.height , 
            contFrame->f->framePos.x + contFrame->spacingWithBox.width + contFrame->posInBox.x + contFrame->contentShowSize.width);

        
    }
}


Frame * getContentFrameFrame(void * contentFrame){
    ContentFrame * contFrame = (ContentFrame *) contentFrame;

    return contFrame->f;
}


void updateSizeContentFrame(void * contentFrame){
    ContentFrame * contFrame = (ContentFrame *) contentFrame;
    delwin(contFrame->winBox);
    refresh();

    updateSizeFrame((void *) contFrame->f);
    copySize(&contFrame->f->frameSize, &contFrame->winBoxSize);

    contFrame->winBox = newwin(contFrame->winBoxSize.height, contFrame->winBoxSize.width,
        contFrame->f->framePos.y, contFrame->f->framePos.x);
    printContentFrameBox(contFrame);

    if(contFrame->contentSize.height < contFrame->winBoxSize.height - 
        contFrame->spacingWithBox.height - 1){

        contFrame->f->canMoveUp = False;
        contFrame->f->canMoveDown = False;
        contFrame->contentShowSize.height = contFrame->contentSize.height;
        contFrame->posInBox.y =
            allVerticalPositionFunc[contFrame->verticalPostionInTheBox](
            contFrame->contentSize.height, contFrame->winBoxSize.height -
            contFrame->spacingWithBox.height*2);
    }else{

        if(contFrame->showPosInContent.y == 0){
            contFrame->f->canMoveUp = False;
        } else {
            contFrame->f->canMoveUp = True;
        }
        contFrame->f->canMoveDown = True;
        contFrame->posInBox.y = 0;
        contFrame->contentShowSize.height = contFrame->winBoxSize.height - 
            contFrame->spacingWithBox.height - 1;
    }
    
    if(contFrame->contentSize.width < contFrame->winBoxSize.width - 
        contFrame->spacingWithBox.width - 1){

        contFrame->f->canMoveLeft = False;
        contFrame->f->canMoveRight = False;
        contFrame->contentShowSize.width = contFrame->contentSize.width;
        contFrame->posInBox.x =
            allHorizontalPositionFunc[contFrame->horizontalPositionInTheBox](
            contFrame->contentSize.width, contFrame->winBoxSize.width -
            contFrame->spacingWithBox.width*2);
    }else{

        if(contFrame->showPosInContent.x == 0){
            contFrame->f->canMoveLeft = False;
        } else {
            contFrame->f->canMoveLeft = True;
        }
        contFrame->f->canMoveRight = True;
        contFrame->posInBox.x = 0;
        contFrame->contentShowSize.width = contFrame->winBoxSize.width - 
            contFrame->spacingWithBox.width - 1;
    }

    if(contFrame->showPosInContent.x + contFrame->contentShowSize.width 
       >= contFrame->contentSize.width){

        contFrame->f->canMoveRight = False;
        contFrame->showPosInContent.x = contFrame->contentSize.width 
            - contFrame->contentShowSize.width;
    }
    if(contFrame->showPosInContent.y + contFrame->contentShowSize.height
       >= contFrame->contentSize.height){

        contFrame->f->canMoveDown = False;
        contFrame->showPosInContent.y = contFrame->contentShowSize.height
            - contFrame->contentShowSize.height;
    }

}


boolean isContentFrameSelected(void * contentFrame, int key){
    ContentFrame * contFrame = (ContentFrame * ) contentFrame;

    if(isFrameSelected((void *) contFrame->f, key)){
        printContentFrame((void *) contFrame);
        return True;
    }

    return False;
}


void unselectContentFrame(void * contentFrame){
    ContentFrame * contFrame = (ContentFrame * ) contentFrame;

    unselectFrame((void *) contFrame->f);
    printContentFrame((void *) contFrame);
}


void inputKeyContentFrame(void * contentFrame, int key){
    ContentFrame * contFrame = (ContentFrame * ) contentFrame;

    if(contFrame->f->isFrameVisible && contFrame->f->isFrameSelected){
        if(contFrame->showPosInContent.x > 0 && key == KEY_LEFT){

            contFrame->showPosInContent.x--;
            if(contFrame->showPosInContent.x == 0){
                contFrame->f->canMoveLeft = False;
            }
            if(contFrame->showPosInContent.x + contFrame->contentShowSize.width + 1 
               < contFrame->contentSize.width){
                contFrame->f->canMoveRight = True;
            }
            printContentFrame((void *) contFrame);
        }else if(contFrame->showPosInContent.y > 0 && key == KEY_UP){

            contFrame->showPosInContent.y--;
            if(contFrame->showPosInContent.y == 0){
                contFrame->f->canMoveUp = False;
            }
            if(contFrame->showPosInContent.y + contFrame->contentShowSize.height + 1 
               < contFrame->contentSize.height){
                contFrame->f->canMoveDown = True;
            }
            printContentFrame((void *) contFrame);
        }else if(contFrame->showPosInContent.x + contFrame->contentShowSize.width + 1 
                 < contFrame->contentSize.width && key == KEY_RIGHT){
                
            contFrame->showPosInContent.x++;
            if(contFrame->showPosInContent.x + contFrame->contentShowSize.width + 1 
               == contFrame->contentSize.width){
                contFrame->f->canMoveRight = False;
            }
            if(contFrame->showPosInContent.x > 0){
                contFrame->f->canMoveLeft = True;
            }
            printContentFrame((void *) contFrame);
        }else if(contFrame->showPosInContent.y + contFrame->contentShowSize.height + 1
                 < contFrame->contentSize.height && key == KEY_DOWN){

            contFrame->showPosInContent.y++;
            if(contFrame->showPosInContent.y + contFrame->contentShowSize.height + 1 
               == contFrame->contentSize.height){
                contFrame->f->canMoveDown = False;
            }
            if(contFrame->showPosInContent.y > 0){
                contFrame->f->canMoveUp = True;
            }
            printContentFrame((void *) contFrame);
        }
    }
}


void printContentFrameBox(ContentFrame * contFrame){

    wattr_set(contFrame->winBox, 0, contFrame->f->frameColor, NULL);
    box(contFrame->winBox, 0, 0);
    wattr_set(contFrame->winBox, 0, allColors[Black][White], NULL);

    wrefresh(contFrame->winBox);
}


// Cette fonction sert pour tester la structures ContentFrame et peut servir
// d'example
void testContentFrameMain(){
    // Pour l'instant elle fait rien car il faut que keyList soit finit
}