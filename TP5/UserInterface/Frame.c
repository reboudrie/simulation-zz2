//#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <wchar.h>

#include "Frame.h"
#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "color.h"
#include "Screen.h"


verticalPositionFuncType allVerticalPositionFunc[]={
    getSpaceCenterVertPos,
    getSpaceTopVertPos,
    getSpaceBottomVertPos
};

horizontalPositionFuncType allHorizontalPositionFunc[]={
    getSpaceCenterHoriPos,
    getSpaceLeftHoriPos,
    getSpaceRightHoriPos
};

Frame * createFrame(int frameId, coordinatePercent framePosInPercent, 
                    sizePercent frameSizeInPercent){

    Frame * f = calloc(1, sizeof(Frame));

    f->frameId = frameId;
    f->frameIdPosInFrame.x = 0;
    f->frameIdPosInFrame.y = 0;
    f->frameColor = allColors[Black][White];
    f->arrowSpacing = 1;
    f->canMoveDown = False;
    f->canMoveUp = False;
    f->canMoveLeft = False;
    f->canMoveRight = False;
    f->isFrameSelected = False;
    f->isFrameVisible = False;

    copyCoordinatePercent(&framePosInPercent, &f->framePosPercent);
    copySizePercent(&frameSizeInPercent, &f->frameSizePercent);

    f->framePos.x = f->framePosPercent.x * (float) COLS;
    f->framePos.y = f->framePosPercent.y * (float) LINES;
    f->frameSize.height = f->frameSizePercent.height * (float) LINES;
    f->frameSize.width = f->frameSizePercent.width * (float) COLS;

    return f;
}


void freeFrame(void ** frame){

    free(*frame);
    *frame = NULL;
}


void printFrame(void * frame){

    Frame * f = (Frame *) frame;
    
    if(f->isFrameVisible){

        // On selectione la couleur en fonction de si la frame es sélectionnée
        if(f->isFrameSelected){
            attr_set(0, getReverseColor(f->frameColor), NULL);
        }else{
            attr_set(0, f->frameColor, NULL);
        }

        // On affiche le frameId
        if(f->frameId > KEY_F0 && f->frameId < KEY_F(64)){
            mvwprintw(stdscr, f->frameIdPosInFrame.y + f->framePos.y, 
                      f->frameIdPosInFrame.x + f->framePos.x, 
                      "F%d", f->frameId - KEY_F0);
        }else if(f->frameId >= (int) 'a' && f->frameId <= (int) 'z'){
            mvwprintw(stdscr, f->frameIdPosInFrame.y + f->framePos.y, 
                      f->frameIdPosInFrame.x + f->framePos.x, 
                      "%c", 'A' + f->frameId);
        }else if((f->frameId >= (int) 'A' && f->frameId <= (int) 'Z') ||
                 (f->frameId >= (int) '1' && f->frameId <= (int) '9')){
            mvwprintw(stdscr, f->frameIdPosInFrame.y + f->framePos.y, 
                      f->frameIdPosInFrame.x + f->framePos.x, 
                      "%c", f->frameId);
        }

        // Si la frame était sélectionné on désactive la couleur
        if(f->isFrameSelected){
            attr_set(0, f->frameColor, NULL);
        }

        if(f->canMoveLeft){

            mvaddnstr(f->frameSize.height/2, f->arrowSpacing, "◀", 3);
        }else{
            mvaddnstr(f->frameSize.height/2, f->arrowSpacing, " ", 1);
        }


        if(f->canMoveRight){
            mvaddnstr(f->frameSize.height/2, 
                      f->frameSize.width - 2*f->arrowSpacing, "▶", 3);
        }else{
            mvaddnstr(f->frameSize.height/2, 
                          f->frameSize.width - 2*f->arrowSpacing, " ", 1);
        }

        if(f->canMoveUp){
            mvaddnstr(f->arrowSpacing, f->frameSize.width/2, "▲", 3);
        }else{
            mvaddnstr(f->arrowSpacing, f->frameSize.width/2, " ", 1);
        }

        if(f->canMoveDown){
            mvaddnstr(f->frameSize.height - 2*f->arrowSpacing, 
                      f->frameSize.width/2, "▼", 3);
        }else {
            mvaddnstr(f->frameSize.height - 2*f->arrowSpacing, 
                          f->frameSize.width/2, " ", 1);
        }

        refresh();
    }
}


void updateSizeFrame(void * frame){

    Frame * f = (Frame *) frame;

    f->framePos.x = f->framePosPercent.x * COLS;
    f->framePos.y = f->framePosPercent.y * LINES;
    f->frameSize.height = f->frameSizePercent.height * LINES;
    f->frameSize.width = f->frameSizePercent.width * COLS;
}


boolean isFrameSelected(void * frame, int key){

    Frame * f = (Frame *) frame;

    if(f->isFrameVisible && key == f->frameId){
        f->isFrameSelected = True;
        return True;
    }

    return False;
}


void unselectFrame(void * frame){

    Frame * f = (Frame *) frame;

    if(f->isFrameSelected){
        f->isFrameSelected = False;
    }
}

int getSpaceCenterVertPos(int elemSize, int envSize){
    return (envSize-elemSize)/2;
}

int getSpaceTopVertPos(int elemSize, int envSize){
    (void) elemSize;
    (void) envSize;
    return 0;
}

int getSpaceBottomVertPos(int elemSize, int envSize){
    return envSize-elemSize;
}

int getSpaceCenterHoriPos(int elemSize, int envSize){
    return (envSize-elemSize)/2;
}

int getSpaceLeftHoriPos(int elemSize, int envSize){
    (void) elemSize;
    (void) envSize;
    return 0;
}

int getSpaceRightHoriPos(int elemSize, int envSize){
    return envSize-elemSize;
}