#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "DefaultLib/boolean.h"
#include "DefaultLib/mt19937ar.h"
#include "DefaultLib/position.h"
#include "UserInterface/Screen.h"
#include "UserInterface/terminalUI.h"
#include "UserInterface/keyFrame.h"
#include "Simulation/Grid.h"
#include "Simulation/Simulation.h"

int main(){

    initTerminalUI();
    initTerrainColor();
    /*init mt19937ar*/
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    //Initialisation Simulation
    Screen * scr = initScreen();
    boolean runProg = True;
    SimulationProperties props;
    props.GridSize = createSize(40, 140);
    props.nbSheeps = 50;
    props.nbWolves = 25;
    props.timeBtwSimuAct = 1;

    keyList * keyl = initSimulation(&props, &scr, &runProg);
    
    // Affichage et boucle de la simulation
    printScreen(scr);
    nodelay(stdscr, TRUE);
    int key;
    while(runProg){
        key = getch();
        
        updateSimulation(&props);
        screenCheckAndUpdate(scr, key);
        checkKeys(&keyl, key);
    }
    nodelay(stdscr, FALSE);

    freeSimulation(&props);
    freeTerminalUI();

    return 0;
}