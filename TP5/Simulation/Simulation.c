#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "Simulation.h"
#include "Grid.h"
#include "SheepEntity.h"
#include "Simulation_entity.h"
#include "WolfEntity.h"
#include "../inputFunctions.h"
#include "../UserInterface/ContentFrame.h"
#include "../DefaultLib/mt19937ar.h"


keyList * initSimulation(SimulationProperties * props, Screen ** scr, boolean * SimulationBool){

    props->scr = scr;

    //Création Frame de la Grid
    size contentSize = props->GridSize;
    ContentFrame * cFrame = createContentFrame(KEY_F(2), createCoordinatePercent(0., 0.),
        createSizePercent(1., 1.), contentSize, createSize(2, 2), 
        centerVert, centerHori);
    cFrame->f->isFrameSelected = True;
    Grid * gr = createGrid(contentSize, cFrame);
    props->gr = gr;
    addFrameToScreen(scr, (void *) cFrame, contentFrame);
    Grid * tmpGr = createGrid(contentSize, NULL);
    props->tmpGrid = tmpGr;

    //Ajout Frame quitter simulation
    Frame * keyF = createFrame(KEY_F(3), createCoordinatePercent(0., 0.), createSizePercent(0., 0.));
    keyF->scr = scr;
    int statesSize[] = {1};
    keyList * keyl = initKeyList(keyF, statesSize, 1);
    addKey(keyl, 0, 0, KEY_F(1), "Quitter (f1)", 13, &quit, SimulationBool, False);
    updateKeyFrame(keyl);

    //Ajout des moutons dans la simulation
    for(int i=0; i < props->nbSheeps; i++){
        coordinate sheepPos;
        boolean canPlace = False;

        while(!canPlace){
            sheepPos.x = genrand_real2()*props->GridSize.width;
            sheepPos.y = genrand_real2()*props->GridSize.height;

            if(gr->allEntity[sheepPos.y][sheepPos.x].type == Nothing){
                canPlace = True;
            }
        }
        
        createSheepEntity(gr, sheepPos);
    }

    //Ajout des loups dans la simulation
    for(int i=0; i < props->nbWolves; i++){
        coordinate wolfPos;
        boolean canPlace = False;

        while(!canPlace){
            wolfPos.x = genrand_real2()*props->GridSize.width;
            wolfPos.y = genrand_real2()*props->GridSize.height;

            if(gr->allEntity[wolfPos.y][wolfPos.x].type == Nothing){
                canPlace = True;
            }
        }
        
        createWolfEntity(gr, wolfPos);
    }
    
    //Met le contenu de la grille dans la grille temporaire qui sera utilisé pour
    //permettre de faire en sorte que toutes intérations des entitées se passent
    //en même temps 
    updateSimuGrid(props, True);

    //Affichage de la Grid dans sa Frame
    printGridToContentFrame(gr);

    //Récupère heure début éxec
    props->lastUpdateTime = time(NULL);

    return keyl;
}

void freeSimulation(SimulationProperties * props){

    freeGrid(&(props->gr));
    freeGrid(&(props->tmpGrid));
}

void updateSimuGrid(SimulationProperties * props, boolean reverse){
    Grid * in = props->tmpGrid;
    Grid * out = props->gr;
    if(reverse){
        in = props->gr;
        out = props->tmpGrid;
    }


    out->day = in->day;
    out->simuTime = in->simuTime;
    for(int i=0; i < props->GridSize.height; i++){
        for(int j=0; j < props->GridSize.width; j++){
            copyEntity(&in->allEntity[i][j], &out->allEntity[i][j]);
            copyTerrainConvertion(&in->terrainConvGrid[i][j], &out->terrainConvGrid[i][j]);
            out->terrainGrid[i][j] = in->terrainGrid[i][j];
        }
    }
}

void updateSimulation(SimulationProperties * props){
    if(time(NULL) - props->lastUpdateTime >= props->timeBtwSimuAct){

        props->lastUpdateTime = time(NULL);
        for(int i=0; i < props->GridSize.height; i++){
            for(int j=0; j < props->GridSize.width; j++){
                //Check si on doit convertir de la terre en herbe
                if(props->gr->terrainConvGrid[i][j].activated &&
                   props->gr->simuTime-props->gr->terrainConvGrid[i][j].timeConv >= GRASS_REGROW_TIME){

                    props->tmpGrid->terrainConvGrid[i][j].activated = False;
                    props->tmpGrid->terrainGrid[i][j] = Grass;
                }

                if(props->gr->allEntity[i][j].type != Nothing){
                    checkDeath(props->gr, props->tmpGrid, &props->gr->allEntity[i][j]);
                    props->tmpGrid->allEntity[i][j].entityAge++;
                    checkReproduction(props->gr, props->tmpGrid, &props->gr->allEntity[i][j]);
                    checkEat(props->gr, props->tmpGrid, &props->gr->allEntity[i][j]);

                    if(!props->gr->allEntity[i][j].isDead){

                        if(allEntitiesRules[props->gr->allEntity[i][j].type].behavior == Predator){
                            coordinate predaNewPos = checkPredatorMove(props->gr, 
                                props->tmpGrid, &props->gr->allEntity[i][j]);
                            //fprintf(stderr, "Moving entity\n");
                            moveEntity(props->tmpGrid, createCoordinate(j, i), predaNewPos);
                        } else if(allEntitiesRules[props->gr->allEntity[i][j].type].behavior == Prey){
                            coordinate preyNewPos = checkPreyMove(props->gr, 
                                props->tmpGrid, &props->gr->allEntity[i][j]);
                            //fprintf(stderr, "Moving entity\n");
                            moveEntity(props->tmpGrid, createCoordinate(j, i), preyNewPos);
                        }
                    } else {
                        props->tmpGrid->allEntity[i][j].type = Nothing;
                        props->gr->allEntity[i][j].type = Nothing;
                    }

                }
            }
        }

        props->tmpGrid->simuTime++;
        if(props->tmpGrid->simuTime%12 == 0){
           props->tmpGrid->day = !props->tmpGrid->day; 
        }
        updateSimuGrid(props, False);
        printGridToContentFrame(props->gr);
        printScreen(*props->scr);
    }
}
