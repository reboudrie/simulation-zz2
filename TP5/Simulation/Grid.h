#ifndef __GRID_H__
#define __GRID_H__

#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "../UserInterface/ContentFrame.h"


typedef struct Entity Entity;

typedef enum TerrainType{
    Grass,
    Dirt,

    NB_Terrain
} TerrainType;

typedef struct TerrainConvertion{
    boolean activated:1;
    long unsigned int timeConv;
} TerrainConvertion;

#define GRASS_REGROW_TIME 12

typedef struct Grid{
    ContentFrame * frame;
    long unsigned int simuTime;

    boolean day:1;
    size size;
    TerrainType ** terrainGrid;
    TerrainConvertion ** terrainConvGrid;

    Entity ** allEntity;
} Grid;

extern int * TerrainTypeColor[2];
extern int TerrainTypeColorDay[NB_Terrain];
extern int TerrainTypeColorNight[NB_Terrain];

void initTerrainColor();
Grid * createGrid(size gridSize, ContentFrame * frame);
void freeGrid(Grid ** gr);
void copyTerrainConvertion(TerrainConvertion * original, TerrainConvertion * destination);
void printGridToContentFrame(Grid * gr);
void eatTerrain(Grid * gr, coordinate coor);


#endif