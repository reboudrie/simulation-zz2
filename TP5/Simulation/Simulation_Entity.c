#include<stdio.h>
#include<stdlib.h>

#include "../DefaultLib/mt19937ar.h"
#include "Simulation_entity.h"
#include "Grid.h"

#include "SheepEntity.h"
#include "WolfEntity.h"

EntityRules allEntitiesRules[NB_Simu_Entity];

void (*copyEntityFunc[NB_Simu_Entity])(void * original, void * destination);

/*
void initEntityColor(){
    EntityColorDay[Nothing] = allColors[Black][White];
    EntityColorDay[Wolf] = allColors[C_WolfDay][White];
    EntityColorDay[Shepherd] = allColors[C_SheepDay][Black];

    EntityColorNight[Nothing] = allColors[Black][White];
    EntityColorNight[Wolf] = allColors[C_WolfNight][White];
    EntityColorNight[Shepherd] = allColors[C_SheepNight][Black];    
}*/

void initAllEntities(){
    initSheepEntity();
    initWolfEntity();
}

void freeAllEntities(){
    freeSheepEntity();
    freeWolfEntity();
}

void copyEntity(Entity * original, Entity * destination){
    destination->type = original->type;

    // ça ne sert à rien de copier l'entité si elle est vide
    if(destination->type != Nothing){
        destination->reproductionTried = original->reproductionTried;
        destination->entityAge = original->entityAge;
        destination->lastEat = original->lastEat;
        destination->entityPos = original->entityPos;
    }
}

void moveEntity(Grid * gr, coordinate origin, coordinate destination){
    if(destination.x < gr->size.width && destination.x >= 0 &&
       destination.y < gr->size.height && destination.y >= 0 &&
       gr->allEntity[origin.y][origin.x].type != Nothing && 
       gr->allEntity[destination.y][destination.x].type == Nothing){

        gr->allEntity[destination.y][destination.x].type = 
            gr->allEntity[origin.y][origin.x].type;
        gr->allEntity[destination.y][destination.x].entityAge = 
            gr->allEntity[origin.y][origin.x].entityAge;
        gr->allEntity[destination.y][destination.x].lastEat = 
            gr->allEntity[origin.y][origin.x].lastEat;
        gr->allEntity[origin.y][origin.x].type = Nothing;
        
    }
}

void searchFood(Grid * oriGr, Grid * outGr, Entity * entity, 
                int searchRange, int * nbFound, coordinate * foodPos){

    *nbFound = 0;

    for(int i=-searchRange; i <= searchRange; i++){
        for(int j=-searchRange; j <= searchRange; j++){
            if(i + entity->entityPos.y < oriGr->size.height && 
                   i + entity->entityPos.y >= 0 && 
                   j + entity->entityPos.x < oriGr->size.width &&
                   j + entity->entityPos.x >=0 &&
                   (j != 0 || i != 0)){
                    
                    if(allEntitiesRules[entity->type].behavior == Predator &&
                       allEntitiesRules[oriGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].type].behavior == Prey &&
                       oriGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].type == 
                       outGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].type){

                        foodPos[*nbFound].x = entity->entityPos.x+j;
                        foodPos[*nbFound].y = entity->entityPos.y+i;
                        (*nbFound)++;
                    } else if(allEntitiesRules[entity->type].behavior == Prey &&
                              oriGr->terrainGrid[entity->entityPos.y+i][entity->entityPos.x+j] == Grass && 
                              oriGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].type == Nothing &&
                              oriGr->terrainGrid[entity->entityPos.y+i][entity->entityPos.x+j] == 
                              outGr->terrainGrid[entity->entityPos.y+i][entity->entityPos.x+j]){

                        foodPos[*nbFound].x = entity->entityPos.x+j;
                        foodPos[*nbFound].y = entity->entityPos.y+i;
                        (*nbFound)++;
                    }

                }
        }
    }

}

void searchPredator(Grid * oriGr, Grid * outGr, Entity * entity, 
                int searchRange, int * nbFound, coordinate * foodPos){

    *nbFound = 0;

    for(int i=-searchRange; i <= searchRange; i++){
        for(int j=-searchRange; j <= searchRange; j++){
            if(i + entity->entityPos.y < oriGr->size.height && 
                   i + entity->entityPos.y >= 0 && 
                   j + entity->entityPos.x < oriGr->size.width &&
                   j + entity->entityPos.x >=0 &&
                   (j != 0 || i != 0)){
                    
                    if(allEntitiesRules[oriGr->allEntity[i][j].type].behavior == Predator &&
                       oriGr->allEntity[i][j].type == outGr->allEntity[i][j].type){

                        foodPos[*nbFound].x = j;
                        foodPos[*nbFound].y = i;
                        (*nbFound)++;
                    }

                }
        }
    }
}

void checkEat(Grid * oriGr, Grid * outGr, Entity * entity){

    if(oriGr->simuTime-entity->lastEat >= allEntitiesRules[entity->type].eatCoolDown){
        int foodFound = 0;
        coordinate foodFoundCoor[9];

        searchFood(oriGr, outGr, entity, 1, &foodFound, foodFoundCoor);

        if(allEntitiesRules[entity->type].behavior == Predator){
            if(foodFound){
                int foodPosRdm = genrand_real2()*(double)(foodFound);
                outGr->allEntity[foodFoundCoor[foodPosRdm].y][foodFoundCoor[foodPosRdm].x].type = Nothing;
                oriGr->allEntity[foodFoundCoor[foodPosRdm].y][foodFoundCoor[foodPosRdm].x].isDead = True;
                outGr->allEntity[entity->entityPos.y][entity->entityPos.x].lastEat = oriGr->simuTime;
            }
        } else if(allEntitiesRules[entity->type].behavior == Prey){
            if(foodFound){
                int foodPosRdm = genrand_real2()*(double)(foodFound);
                eatTerrain(outGr, foodFoundCoor[foodPosRdm]);
                outGr->allEntity[entity->entityPos.y][entity->entityPos.x].lastEat = oriGr->simuTime;
            }
        }

    }
}

boolean checkValidMove(int x, int y, Grid * outGr, Entity * entity){
    if(x >= 0 && y >= 0 && x < outGr->size.width && y < outGr->size.height &&
       (outGr->allEntity[y][x].type == Nothing || 
        (x == entity->entityPos.x && y == entity->entityPos.y))){
        
        return True;
    }

    return False;
}

coordinate checkPredatorMove(Grid * oriGr, Grid * outGr, Entity * entity){
    
    short range = allEntitiesRules[entity->type].range;
    boolean canMoveTo = False;
    coordinate pos;
    int maxTry = 100;

    if(oriGr->simuTime-entity->lastEat >= allEntitiesRules[entity->type].eatCoolDown){
        int foodFound = 0;
        coordinate * foodPos = calloc((2*range+1) * (2*range+1), sizeof(coordinate));
        searchFood(oriGr, outGr, entity, range, &foodFound, foodPos);

        if(foodFound){
            int targetFood = genrand_real2()*(double)(foodFound);
            while(!canMoveTo && maxTry > 0){

                int xTarg = (int)(genrand_real2()*3.)-1;
                int yTarg = (int)(genrand_real2()*3.)-1;
                pos.x = xTarg+foodPos[targetFood].x;
                pos.y = yTarg+foodPos[targetFood].y;

                if(checkValidMove(pos.x, pos.y, outGr, entity) &&
                   abs(pos.x-entity->entityPos.x) <= range &&
                   abs(pos.y-entity->entityPos.y) <= range){
                    
                    canMoveTo = True;
                }

                maxTry--;
            }

            if(maxTry == 0){
                free(foodPos);
                return createCoordinate(entity->entityPos.x, entity->entityPos.y);
            }

            free(foodPos);
            return pos;
        }
    }

    while(!canMoveTo && maxTry > 0){
             
        int xTarg = (int)(genrand_real2()*((double)(2*range+1)))-range;
        int yTarg = (int)(genrand_real2()*((double)(2*range+1)))-range;
        
        pos.x = xTarg+entity->entityPos.x;
        pos.y = yTarg+entity->entityPos.y;

        canMoveTo = checkValidMove(pos.x, pos.y, outGr, entity);
        maxTry--;
    }

    if(maxTry){
        return pos;
    } else {
        return entity->entityPos;
    }
    
}

coordinate checkPreyMove(Grid * oriGr, Grid * outGr, Entity * entity){

    short range = allEntitiesRules[entity->type].range;
    boolean canMoveTo = False;
    coordinate pos;
    int predatorFound = 0;
    coordinate * predatorPos = calloc((2*range+1) * (2*range+1), sizeof(coordinate));
    int maxTry = 100;

    //S'enfuir du prédateur une priorité de la proie
    if(predatorFound){
        //On reagarde les endroits où sont les predateurs afin de choisir
        //aléatoirement le position de fuite
        int * posPredaX = calloc(range*2+1, sizeof(int));
        int * posPredaY = calloc(range*2+1, sizeof(int));
        for(int i=0; i < predatorFound; i++){
            posPredaX[predatorPos->x-entity->entityPos.x+range]++;
            posPredaX[predatorPos->y-entity->entityPos.y+range]++;
        }
        
        int nbEscapeX = 0, nbEscapeY = 0;
        int * escapeX = calloc(range*2+1, sizeof(int));
        int * escapeY = calloc(range*2+1, sizeof(int));
        for(int i=0; i < range*2+1; i++){
            if(!posPredaX[i]){
                escapeX[nbEscapeX] = i-range;
                nbEscapeX++;
            }
            if(!posPredaY[i]){
                escapeX[nbEscapeY] = i-range;
                nbEscapeY++;
            }
        }
        free(posPredaX);
        free(posPredaY);

        if(nbEscapeX != 0 && nbEscapeY != 0){
            while(!canMoveTo && maxTry > 0){
                pos.x = escapeX[(int)(genrand_real2()*(double)(nbEscapeX))]+entity->entityPos.x;
                pos.y = escapeY[(int)(genrand_real2()*(double)(nbEscapeY))]+entity->entityPos.y;

                canMoveTo = checkValidMove(pos.x, pos.y, outGr, entity);
                maxTry--;
            }
            free(escapeX);
            free(escapeY);

            if(maxTry){
                return pos;
            } else {
                return entity->entityPos;
            }
            
        }
        
        free(escapeX);
        free(escapeY);        
    }


    if(oriGr->simuTime-entity->lastEat >= allEntitiesRules[entity->type].eatCoolDown){
        int foodFound = 0;
        coordinate * foodPos = calloc((2*range+1) * (2*range+1), sizeof(coordinate));
        searchFood(oriGr, outGr, entity, range, &foodFound, foodPos);

        if(foodFound){
            int targetFood = genrand_real2()*(double)(foodFound);

            while(!canMoveTo && maxTry > 0){

                int xTarg = (int)(genrand_real2()*3.)-1;
                int yTarg = (int)(genrand_real2()*3.)-1;
                pos.x = xTarg+foodPos[targetFood].x;
                pos.y = yTarg+foodPos[targetFood].y;

                if(checkValidMove(pos.x, pos.y, outGr, entity) &&
                   abs(pos.x-entity->entityPos.x) <= range &&
                   abs(pos.y-entity->entityPos.y) <= range){
                    
                    canMoveTo = True;
                }

                maxTry--;
            }

            if(maxTry == 0){
                free(foodPos);
                return createCoordinate(entity->entityPos.x, entity->entityPos.y);
            }

            free(foodPos);
            return pos;
        }
    }

    while(!canMoveTo && maxTry > 0){
                
        int xTarg = (int)(genrand_real2()*((double)(2*range+1)))-range;
        int yTarg = (int)(genrand_real2()*((double)(2*range+1)))-range;

        pos.x = xTarg+entity->entityPos.x;
        pos.y = yTarg+entity->entityPos.y;

        canMoveTo = checkValidMove(pos.x, pos.y, outGr, entity);
        maxTry--;
    }

    if(maxTry){
        return pos;
    } else {
        return entity->entityPos;
    }
    
}

void checkReproduction(Grid * oriGr, Grid * outGr, Entity * entity){
    int nbRepCheck = 0;
    oriGr->allEntity[entity->entityPos.y][entity->entityPos.x].reproductionTried = True;

    for(int i=-1; i <= 1; i++){
        for(int j=-1; j <= 1; j++){
            if((j != 0 || i != 0) && 
               i + entity->entityPos.y < oriGr->size.height && 
               i + entity->entityPos.y >= 0 && 
               j + entity->entityPos.x < oriGr->size.width &&
               j + entity->entityPos.x >=0 &&
               oriGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].type == 
               entity->type &&
               oriGr->allEntity[entity->entityPos.y+i][entity->entityPos.x+j].reproductionTried ==
               False){
                nbRepCheck++;
            }
        }
    }

    boolean createNewEntity = False;
    for(int i=0; i < nbRepCheck; i++){
        if(allEntitiesRules[entity->type].probabilityReproduction >= genrand_real1()){
            createNewEntity = True;
        }
    }

    if(createNewEntity){
        int range = allEntitiesRules[entity->type].range;

        for(int i=-range; i <= range && createNewEntity; i++){
            for(int j=-range; j <= range && createNewEntity; j++){
                if(checkValidMove(j+entity->entityPos.x, i+entity->entityPos.y, outGr, entity)){
                    if(entity->type == Sheep){
                        createSheepEntity(outGr, createCoordinate(j+entity->entityPos.x, i+entity->entityPos.y));
                    } else if(entity->type == Wolf){
                        createWolfEntity(outGr, createCoordinate(j+entity->entityPos.x, i+entity->entityPos.y));
                    }
                    createNewEntity = False;
                }
            }
        }
    }
}

void checkDeath(Grid * oriGr, Grid * outGr, Entity * entity){
    if(oriGr->simuTime-entity->lastEat >= allEntitiesRules[entity->type].maxTimeWithoutEating){
        outGr->allEntity[entity->entityPos.y][entity->entityPos.x].type = Nothing;
        oriGr->allEntity[entity->entityPos.y][entity->entityPos.x].isDead = True;
    }
    
    if(entity->entityAge >= allEntitiesRules[entity->type].maxAge){
        outGr->allEntity[entity->entityPos.y][entity->entityPos.x].type = Nothing;
        oriGr->allEntity[entity->entityPos.y][entity->entityPos.x].isDead = True;
    }
    
}