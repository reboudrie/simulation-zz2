#ifndef __SIMULATION_H__
#define __SIMULATION_H__

#include <time.h>

#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "../UserInterface/Screen.h"
#include "../UserInterface/keyFrame.h"
#include "Grid.h"

typedef struct SimulationProperties{
    size GridSize;

    int nbSheeps;
    int nbWolves;

    int timeBtwSimuAct;

    Grid * gr;
    Grid * tmpGrid;
    time_t lastUpdateTime;
    Screen ** scr;
} SimulationProperties;

keyList * initSimulation(SimulationProperties * props, Screen ** scr, boolean * SimulationBool);
void freeSimulation(SimulationProperties * props);
void updateSimuGrid(SimulationProperties * props, boolean reverse);
void updateSimulation(SimulationProperties * props);

#endif