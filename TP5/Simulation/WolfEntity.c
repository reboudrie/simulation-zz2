#include <stdlib.h>

#include "../UserInterface/color.h"
#include "WolfEntity.h"
#include "Grid.h"
#include "Simulation_entity.h"

void initWolfEntity(){
    allEntitiesRules[Wolf].range = 1;
    allEntitiesRules[Wolf].behavior = Predator;
    allEntitiesRules[Wolf].maxAge = 20 * 24;
    allEntitiesRules[Wolf].probabilityReproduction = .1;
    allEntitiesRules[Wolf].representation = 'L';
    allEntitiesRules[Wolf].color[True] = allColors[C_WolfDay][Black];
    allEntitiesRules[Wolf].color[False] = allColors[C_WolfNight][Black];
    allEntitiesRules[Wolf].maxTimeWithoutEating = 3 * 24;
    allEntitiesRules[Wolf].eatCoolDown = 6;

}

void freeWolfEntity(){
    //Rien à free
}


void createWolfEntity(Grid * gr, coordinate coor){
    gr->allEntity[coor.y][coor.x].type = Wolf;
    gr->allEntity[coor.y][coor.x].reproductionTried = False;
    gr->allEntity[coor.y][coor.x].isDead = False;
    gr->allEntity[coor.y][coor.x].entityAge = 1;
    gr->allEntity[coor.y][coor.x].lastEat = gr->simuTime;
}