#include<stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "Grid.h"
#include "../UserInterface/color.h"
#include "Simulation_entity.h"


int * TerrainTypeColor[2] = {TerrainTypeColorNight, TerrainTypeColorDay};
int TerrainTypeColorDay[NB_Terrain];
int TerrainTypeColorNight[NB_Terrain];

void initTerrainColor(){
    TerrainTypeColorDay[Grass] = allColors[C_GrassDay][White];
    TerrainTypeColorDay[Dirt] = allColors[C_DirtDay][White];

    TerrainTypeColorNight[Grass] = allColors[C_GrassNight][White];
    TerrainTypeColorNight[Dirt] = allColors[C_DirtNight][White];
}

Grid * createGrid(size gridSize, ContentFrame * frame){
    Grid * gr = calloc(1, sizeof(Grid));
    gr->day = True;
    copySize(&gridSize, &gr->size);
    gr->terrainGrid = calloc(gridSize.height, sizeof(TerrainType *));
    gr->allEntity = calloc(gridSize.height, sizeof(Entity *));
    gr->terrainConvGrid = calloc(gridSize.height, sizeof(TerrainConvertion));
    for(int i=0; i < gridSize.height; i++){
        gr->terrainGrid[i] = calloc(gridSize.width, sizeof(TerrainType));
        gr->allEntity[i] = calloc(gridSize.width, sizeof(Entity));   
        gr->terrainConvGrid[i] = calloc(gridSize.width, sizeof(TerrainConvertion));

        for(int j=0; j < gridSize.width; j++){
            gr->allEntity[i][j].entityPos.x = j;
            gr->allEntity[i][j].entityPos.y = i;
        }
    }
    
    gr->frame = frame;
    initAllEntities();

    return gr;
}

void freeGrid(Grid ** gr){
    for(int i=0; i < (*gr)->size.height; i++){
        free((*gr)->terrainConvGrid[i]);
        free((*gr)->terrainGrid[i]);
        free((*gr)->allEntity[i]);
    }
    free((*gr)->terrainConvGrid);
        free((*gr)->terrainGrid);
        free((*gr)->allEntity);

    free(*gr);
    *gr = NULL;

    freeAllEntities();
}

void copyTerrainConvertion(TerrainConvertion * original, TerrainConvertion * destination){
    destination->activated = original->activated;

    if(destination->activated){
        destination->timeConv = original->timeConv;
    }
}

void printGridToContentFrame(Grid * gr){
    for(int i=0; i < gr->size.height; i++){
        for(int j=0; j < gr->size.width; j++){
            //wattron(gr->frame->content, 
            //        COLOR_PAIR(TerrainTypeColor[gr->day][gr->terrainGrid[i][j]]));
            
            //if(gr->allEntity[i][j].type == Nothing || gr->allEntity[i][j].isDead){
            if(gr->allEntity[i][j].type == Nothing){
                wattr_set(gr->frame->content, 0, 
                          TerrainTypeColor[gr->day][gr->terrainGrid[i][j]], NULL);
                mvwaddnstr(gr->frame->content, i, j,
                           " ", 1);
                wattr_set(gr->frame->content, 0, 0, NULL);
            } else {
                
                wattr_set(gr->frame->content, 0, 
                          allEntitiesRules[gr->allEntity[i][j].type].color[gr->day], NULL);
                //fprintf(stderr, "Type: %d", gr->allEntity[i][j].type);
                mvwaddnstr(gr->frame->content, i, j,
                           &allEntitiesRules[gr->allEntity[i][j].type].representation, 1);
                wattr_set(gr->frame->content, 0, 0, NULL);
            }
            
        }
    }
}

void eatTerrain(Grid * gr, coordinate coor){
    if(gr->terrainGrid[coor.y][coor.x] == Grass){
        gr->terrainGrid[coor.y][coor.x] = Dirt;
        gr->terrainConvGrid[coor.y][coor.x].activated = True;
        gr->terrainConvGrid[coor.y][coor.x].timeConv = gr->simuTime;
    }
}