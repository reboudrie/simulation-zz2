#ifndef __WOLFENTITY_H__
#define __WOLFENTITY_H__

#include "Simulation_entity.h"

void initWolfEntity();
void freeWolfEntity();
void createWolfEntity(Grid * gr, coordinate coor);

#endif