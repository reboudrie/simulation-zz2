#ifndef __SIMULATION_ENTITY_H__
#define __SIMULATION_ENTITY_H__

#include "../DefaultLib/boolean.h"
#include "../DefaultLib/position.h"
#include "../UserInterface/color.h"
#include "Grid.h"

typedef enum EntityType{
    Nothing,
    Sheep,
    Wolf,

    NB_Simu_Entity
} EntityType;

typedef enum EntityBehavior{
    Predator,
    Prey
} EntityBehavior;

typedef struct EntityRules{
    short color[2];
    char representation;
    long unsigned int maxAge;
    EntityBehavior behavior:2;
    unsigned int maxTimeWithoutEating;
    unsigned int eatCoolDown;
    short range;
    double probabilityReproduction;

} EntityRules;

typedef struct Entity{
    EntityType type:3;
    boolean reproductionTried:1;
    boolean isDead:1;

    long unsigned int entityAge;
    long unsigned int lastEat;
    coordinate entityPos;
} Entity;

extern EntityRules allEntitiesRules[NB_Simu_Entity];

void initAllEntities();
void freeAllEntities();
void copyEntity(Entity * original, Entity * destination);
void moveEntity(Grid * gr, coordinate origin, coordinate destination);
void checkEat(Grid * oriGr, Grid * outGr, Entity * entity);
coordinate checkPredatorMove(Grid * oriGr, Grid * outGr, Entity * entity);
coordinate checkPreyMove(Grid * oriGr, Grid * outGr, Entity * entity);
void checkReproduction(Grid * oriGr, Grid * outGr, Entity * entity);
void checkDeath(Grid * oriGr, Grid * outGr, Entity * entity);

#endif