#ifndef __SHEEPENTITY_H__
#define __SHEEPENTITY_H__

#include "Simulation_entity.h"

void initSheepEntity();
void freeSheepEntity();
void createSheepEntity(Grid * gr, coordinate coor);

#endif