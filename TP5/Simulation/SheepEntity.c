#include <stdio.h>
#include <stdlib.h>

#include "../UserInterface/color.h"
#include "SheepEntity.h"
#include "Grid.h"
#include "Simulation_entity.h"

void initSheepEntity(){
    allEntitiesRules[Sheep].range = 1;
    allEntitiesRules[Sheep].behavior = Prey;
    allEntitiesRules[Sheep].maxAge = 10 * 24;
    allEntitiesRules[Sheep].probabilityReproduction = .01;
    allEntitiesRules[Sheep].representation = 'M';
    allEntitiesRules[Sheep].color[True] = allColors[C_SheepDay][Black];
    allEntitiesRules[Sheep].color[False] = allColors[C_SheepNight][Black];
    allEntitiesRules[Sheep].maxTimeWithoutEating = 3 * 24;
    allEntitiesRules[Sheep].eatCoolDown = 3;

}

void freeSheepEntity(){
    //Rien à free
}

void createSheepEntity(Grid * gr, coordinate coor){
    gr->allEntity[coor.y][coor.x].type = Sheep;
    gr->allEntity[coor.y][coor.x].reproductionTried = False;
    gr->allEntity[coor.y][coor.x].isDead = False;
    gr->allEntity[coor.y][coor.x].entityAge = 1;
    gr->allEntity[coor.y][coor.x].lastEat = gr->simuTime;
}