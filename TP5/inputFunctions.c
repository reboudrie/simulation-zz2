#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "inputFunctions.h"
#include "DefaultLib/boolean.h"
#include "UserInterface/keyFrame.h"
#include "UserInterface/Screen.h"

void quit(keyList ** keyl, void * input){
    boolean * programRun = (boolean *) input;
    *programRun = False;

    freeScreen((*keyl)->frame->scr, True);
    freeKeyList(keyl, True);
}
