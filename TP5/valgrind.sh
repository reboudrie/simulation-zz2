#!/bin/bash

valgrind --leak-check=full --show-leak-kinds=all -s ./Prog 2> valgrind.log
