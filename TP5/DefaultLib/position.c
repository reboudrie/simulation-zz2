#include <stdio.h>

#include "position.h"

coordinate createCoordinate(int x, int y){
    coordinate C = {x, y};
    return C;
}

void copyCoordinate(coordinate * origin, coordinate * destination){
    destination->x = origin->x;
    destination->y = origin->y;
}

coordinatePercent createCoordinatePercent(float x, float y){
    coordinatePercent C = {x, y};
    return C;
}

void copyCoordinatePercent(coordinatePercent * origin, coordinatePercent * destination){
    destination->x = origin->x;
    destination->y = origin->y;
}

size createSize(int height, int width){
    size S = {height, width};
    return S;
}

void copySize(size * origin, size * destination){
    destination->height = origin->height;
    destination->width = origin->width;
}

sizePercent createSizePercent(float height, float width){
    sizePercent S = {height, width};
    return S;
}

void copySizePercent(sizePercent * origin, sizePercent * destination){
    destination->height = origin->height;
    destination->width = origin->width;
}

int abs(int val){
    return (val < 0) ? -val : val;
}