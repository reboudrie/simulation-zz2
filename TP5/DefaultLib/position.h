#ifndef __POSITION_H__
#define __POSITION_H__

typedef struct coordinate{
    int x;
    int y;
} coordinate;

typedef struct coordinatePercent{
    float x;
    float y;
} coordinatePercent;

typedef struct size{
    int height;
    int width;
} size;

typedef struct sizePercent{
    float height;
    float width;
} sizePercent;

coordinate createCoordinate(int x, int y);

void copyCoordinate(coordinate * origin, coordinate * destination);

coordinatePercent createCoordinatePercent(float x, float y);

void copyCoordinatePercent(coordinatePercent * origin, coordinatePercent * destination);

size createSize(int height, int width);

void copySize(size * origin, size * destination);

sizePercent createSizePercent(float height, float width);

void copySizePercent(sizePercent * origin, sizePercent * destination);

int abs(int val);

#endif