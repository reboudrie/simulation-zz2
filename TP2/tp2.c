#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mt19937ar.h"
#include "tp2.h"

double uniform(double a, double b){
    return (b-a)*genrand_real1()+a; 
}

double * cumulativeProbabiltyPopulation(int size, double * populations){

    //Calculate the total number of indivals
    double totIndiv = 0;
    for(int i=0; i<size; i++){
        totIndiv += populations[i];
    }

    /* 
     *  This whole section is commented because it is in the statement but I choose not to use it
     *  because using the probability of being in each population to calculate the cumulative probability
     *  can cause some problems due to the way double is stored in memory. And so for a big number of
     *  populations the last element of the array of cumulative probability would most certainly not
     *  have been 1 due to the propagation of the error.
     */ 
    /*// Create and calculate an array of the probability to be in each population
    double * probaPop = calloc(size, sizeof(double));
    for(int i=0; i<size; i++){
        probaPop[i] = populations[i]/totIndiv;
    }*/

    //Create and calculate an array of the cumulate probability to be in each population
    double * cumulProbaPop = calloc(size, sizeof(double));
    double cumul = 0;
    for(int i=0; i<size; i++){
        cumul += populations[i];
        cumulProbaPop[i] = cumul/totIndiv;

    }

    return cumulProbaPop;
}

int * drawPopulation(int nbDraw, int size, double populations[]){
    int * totDraw = calloc(size, sizeof(int));

    double * cumulProba = cumulativeProbabiltyPopulation(size,populations);
    
    for(int i=0; i<nbDraw; i++){
        double draw = genrand_real1();

        int position = 0;
        while(draw > cumulProba[position] && position<size){
            position++;
        }
        
        totDraw[position]++;
    }

    return totDraw;
}

double negExp(double mean){
    return -mean * log(1 - genrand_real1());
}

void boxAndMuller(double * x1, double * x2){
    double randNumb1 = genrand_real1();
    double randNumb2 = genrand_real1();

    *x1 = cos(2. * M_PI * randNumb2) * pow(-2. * log(randNumb1), 1./2.);
    *x1 = sin(2. * M_PI * randNumb2) * pow(-2. * log(randNumb1), 1./2.);
}

void TP2Question2(int nbVal, double tempMin, double tempMax){
    for(int i=0; i<nbVal; i++){
        fprintf(stdout, "%d\t temperature: %.1f\n", i, uniform(tempMin, tempMax));
    }
}

void TP2Question3(){

    //Question A
    fprintf(stdout, "Question A:\n\n");

    double classeABC[] = {350, 450, 200};
    int nbDrawA[] = {1000, 10000, 100000, 1000000};
    
    for(int i=0; i<4; i++){
        int * totDraw = drawPopulation(nbDrawA[i], 3, classeABC);
        fprintf(stdout, "for %d draws:\nclasse 1: %d, class 2: %d, class 3: %d\n\n", 
                nbDrawA[i], totDraw[0], totDraw[1], totDraw[2]);
    }


    //Question B
    fprintf(stdout, "Question B:\n\n");

    double HDL[] = {100, 400, 600, 400, 100, 200};
    int nbDrawB[] = {1000, 1000000};

    for(int i=0; i<2; i++){
        int * totDraw = drawPopulation(nbDrawB[i], 6, HDL);
        fprintf(stdout, "for %d draws:\nclasse 1: %d, class 2: %d, class 3: %d, class 4: %d, class 5: %d, class 6: %d\n\n",
                nbDrawB[i], totDraw[0], totDraw[1], totDraw[2], totDraw[3], totDraw[4], totDraw[5]);
    }
}

void TP2Question4(){
    //Question B
    fprintf(stdout, "Question B:\n\n");

    int nbDrawB[] = {1000, 1000000};
    double mean = 10.;

    for(int i=0; i<2; i++){
        double cumul = 0.;
        for(int j=0; j<nbDrawB[i]; j++){
            cumul += negExp(mean);
        }
        fprintf(stdout, "For %d draw, mean value: %f\n", nbDrawB[i], cumul/(double)nbDrawB[i]);
    }

    //Question C
    fprintf(stdout, "\nQuestion C:\n\n");

    int nbDrawC[] = {1000, 1000000};

    for(int i=0; i<2; i++){
        int * freq = calloc(21, sizeof(int));
        for(int j=0; j<nbDrawC[i]; j++){
            double draw = negExp(mean);
            if(draw < 20.){
                freq[(int) draw]++;
            }else{
                freq[20]++;
            }
        }
        fprintf(stdout, "For %d draw:\n", nbDrawC[i]);
        for(int j=0; j<19; j++){
            fprintf(stdout, "%d draws between %d and %d\n", freq[j], j, j+1);
        }
        fprintf(stdout, "%d draws above 20\n\n", freq[20]);
        free(freq);
    }
}

void TP2Question5_1(){
    int * sumStor = calloc(201,sizeof(int));
    int nbDraw = 10000000;
    double diceProba[] = {1.,1.,1.,1.,1.,1.};

    for(int i=0; i<nbDraw; i++){
        int sum = 0;
        int * drawResult = drawPopulation(40,6,diceProba);
        for(int j=0; j<6; j++){
            sum += drawResult[j]*(j+1);
        }
        free(drawResult);
        sumStor[sum-40]++;
    }

    for(int i=0; i<201; i++){
        fprintf(stdout, "%03d: %d\n", i+40, sumStor[i]);
    }
}

void TP2Question5_2(){
    int * freq = calloc(21,sizeof(int));
    int nbDraw = 10000000;

    for(int i=0; i<nbDraw; i++){
        double x1 = 0., x2 = 0.;
        boxAndMuller(&x1, &x2);
        if((x1*2.+10.) > 0 && (x1*2.+10.) < 20){
            freq[(int) (x1*2.+10.)]++;
            freq[(int) (x2*2.+10.)]++;
        }
    }

    for(int i=0; i<21; i++){
        fprintf(stdout, "%.1f,%f\n", ((double)i-10.)/2., (double)freq[i]/1000000);
    }
}

int main(int argc, char * argv[]){

    // initialise the random number generator
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);

    //Question 2 of TP2
    //TP2Question2(20, -98., 57.7);

    //Question 3 of TP2
    //TP2Question3();

    //Question 4 of TP2
    //TP2Question4();

    //Question 5.1 of TP2
    //TP2Question5_1();

    //Question 5.2 of TP2
    //TP2Question5_2();

    return 0;
}