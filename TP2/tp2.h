#ifndef __TP2_H__
#define __TP2_H__

/**--------------------------------------------------------------
 * 
 * @brief           function that returns a random number between a and b
 * 
 * @param   a       the beginnig of the intervalle
 * @param   b       the end of the intervalle
 * @return  double  the random number generated
 * 
 ----------------------------------------------------------------*/
double uniform(double a, double b);

/**--------------------------------------------------------------
 * 
 * @brief                   function that returns the array of
 *                          cumulative probability of the poupulation
 *                          put in iput
 * 
 * @param   size            number of population
 * @param   populations     number of individuals in the population n
 * @return  double *        the cumulative probability to be in each population
 * 
 ---------------------------------------------------------------*/
double * cumulativeProbabiltyPopulation(int size, double * populations);

/**--------------------------------------------------------------
 * @brief                   function that returns an array of
 *                          the total number of draws of the
 *                          population n of the array Populations
 * 
 * @param   nbDraw          the number of draws to do
 * @param   size            the number of populations in the array populations
 * @param   populations     the number of individuals in the population n
 * @return  int*            the array of the number of draw each population got
 * 
 ---------------------------------------------------------------*/
int * drawPopulation(int nbDraw, int size, double populations[]);

/**--------------------------------------------------------------
 * @brief               return the random value of the negative exponential
 *                      with a mean value of mean
 * 
 * @param   mean        the mean value to apply to the negative exponential function
 * @return  double      the random value returned
 * 
 ---------------------------------------------------------------*/
double negExp(double mean);

/**
 * @brief           draw two random numbers that follow a centred and
 *                  reduced Gaussian law
 * 
 * @param   x1      the first number draw
 * @param   x2      the second number draw
 */
void boxAndMuller(double * x1, double * x2);

/**-------------------------------------------------------------
 * 
 * @brief Function to answer the question 2 of the TP2
 * 
 * @param nbVal     number of values to print
 * @param tempMin   minimal temperature 
 * @param tempMax   maximal temperature
 * 
 ---------------------------------------------------------------*/
void TP2Question2(int nbVal, double tempMin, double tempMax);

#endif