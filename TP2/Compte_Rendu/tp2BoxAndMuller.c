void boxAndMuller(double * x1, double * x2){
    double randNumb1 = genrand_real1();
    double randNumb2 = genrand_real1();

    *x1 = cos(2. * M_PI * randNumb2) * pow(-2. * log(randNumb1), 1./2.);
    *x1 = sin(2. * M_PI * randNumb2) * pow(-2. * log(randNumb1), 1./2.);
}