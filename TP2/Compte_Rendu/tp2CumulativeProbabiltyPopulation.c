double * cumulativeProbabiltyPopulation(int size, double * populations){

    //Calculate the total number of indivals
    double totIndiv = 0;
    for(int i=0; i<size; i++){
        totIndiv += populations[i];
    }

    //Create and calculate an array of the cumulate probability to be in each population
    double * cumulProbaPop = calloc(size, sizeof(double));
    double cumul = 0;
    for(int i=0; i<size; i++){
        cumul += populations[i];
        cumulProbaPop[i] = cumul/totIndiv;

    }

    return cumulProbaPop;
}