#ifndef __TP4_H__
#define __TP4_H__

#include <stdio.h>

#include "boolean.h"

typedef enum Sex{
    Male,
    Female
} Sex;

typedef struct youngRabbit{
    unsigned int numberOfRabbit;                    //Number of young rabbit that corresponds
                                                    //to this rabbit

    Sex sex:1;                                      //Sex of the young rabbit
} youngRabbit;

typedef struct adultRabbit{
    unsigned int numberOfRabbit;                    //Number of young rabbit that corresponds
                                                    //to this rabbit

    Sex sex:1;                                      //Sex of the young rabbit

    unsigned int numberOfLitter:4;                  //Number of litter this rabbit had this year
} adultRabbit;

typedef struct rabbitTable{
    youngRabbit * allYoung;                         //Table of every young rabbit possible. Every
                                                    //rabbit in even position is a Male and every
                                                    //rabbit in odd position is a Female

    unsigned int sizeYoung;                         //Size of the table of every young rabbit

    adultRabbit ** allAdult;                        //2D table of every adult rabbit possible. The
                                                    //first index correspond to the age which the
                                                    //rabbit became an adult. The second index is
                                                    //organised with first the Male rabbit of this age
                                                    //and after there is Female rabbit with 0 litter
                                                    //this year and after 1 litter and so on.

    unsigned int * sizeAdult;                       //Table that contains the size of each line of
                                                    //the allAdult table

    unsigned int sizeMature;                        //Number of line in the table allAdult

    unsigned int totalAdultRabbit;                  //The sum of the content of sizeAdult

    unsigned int * sizeAdultYear;                   //The table that contains the number of rabbits
                                                    //that is needed for the x month of the year in
                                                    //the table allAdult
} rabbitTable;

typedef struct rabbitStatistic{
    long unsigned int totalRabbit;                      //Total number of rabbits

    long unsigned int numberYoungRabbit;                //Total number of young rabbits

    long unsigned int numberAdultRabbit;                //Total number of adult rabbits

    long unsigned int numberYoungMaleRabbit;            //Total number of young male rabbits

    long unsigned int numberYoungFemaleRabbit;          //Total number of young female rabbits

    long unsigned int numberAdultMaleRabbit;            //Total number of adult male rabbits

    long unsigned int numberAdultFemaleRabbit;          //Total number of adult female rabbits

    long unsigned int * numberFemaleRabbitPerLitter;    //Table of the total of all the adult female 
                                                        //rabbits that had x litter

    long unsigned int * numberMaleRabbitPerAge;         //Table of the total of all male rabbits that
                                                        //is x month old

    long unsigned int * numberFemaleRabbitPerAge;       //Table of the total of all female rabbits that
                                                        //is x month old
} rabbitStatistic;

/*Probability of a rabbit to be a male*/
extern double probaSexMale;

/*Probability of a young rabbit to survive*/
extern double probaSurvYoung;

/*Probabilty of an adult rabbit to survive*/
extern double probaSurvAdult;

/*Age in month when the rabbit survival chances start to decrease*/
extern unsigned int ageStartSurvDecrease;

/*Decrease value of rabbit above the age of ageStartSurvDecrease*/
extern double probaSurvDecrease;

/*Spacing between each decrease of survival chances for a rabbit above the 
age of ageStartSurvDecrease*/
extern unsigned int spacingSurvDecrease;

/*Probability of an adult rabbit to survive each year after the rabbit
is older than  ageStartSurvDecrease*/
extern double * probaSurv;

/*Maximal age in month that a rabbit can survive to*/
extern unsigned int maxSurvAge;

/*Age in month to which the maturity of rabbit can start*/
extern unsigned int ageMaturityStart;

/*Number of month that the maturity of a rabbit can happen*/
extern unsigned int numberMonthMaturity;

/*Table of the minimum and maximum of litter a female can give per year*/
extern unsigned int femaleNumberLitterPerYear[2];

/*Table of the minimum and maximum of number of rabbit a litter can create*/
extern unsigned int numberBabyPerLitter[2];

/*Table of probability that correspond to the probability of a certain number 
rabbit to be born*/
extern double * probaNumberBaby;

/**----------------------------------------------------------------------------
 *
 * @brief       Initialise all the global variables for the rabbits
 * 
 ----------------------------------------------------------------------------*/
void initAllRabbitStat();

/**----------------------------------------------------------------------------
 *
 * @brief       Free all the global variables for the rabbits
 * 
 ----------------------------------------------------------------------------*/
void freeAllRabbitStat();

/**----------------------------------------------------------------------------
 *
 * @brief                   Create and initialise every rabbit possible and
 *                          returns it
 * 
 * @return rabbitTable*     Returns a pointer to a structure that contains 
 *                          every rabbit possible 
 *
 ----------------------------------------------------------------------------*/
rabbitTable * genAllRabbit();

/**----------------------------------------------------------------------------
 *
 * @brief               Free the structure of every rabbit possible
 * 
 * @param allRabbit     Address of a pointer that contains a structure of 
 *                      every rabbit possible  
 *
 ----------------------------------------------------------------------------*/
void freeAllRabbit(rabbitTable ** allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief               Print the memory that is used to store all the 
 *                      possible rabbits
 * 
 * @param file          File where you want to write the memory used to 
 *                      store all the possible rabbits
 * @param allRabbit     Pointer to a structure that contains all the 
 *                      possible rabbits
 *
 ----------------------------------------------------------------------------*/
void printRabbitMemory(FILE * file, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief                   Get the age of the young rabbit in input
 * 
 * @param rabbit            Pointer to the rabbit that you want the age of
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 *
 * @return unsigned int     The age of the young rabbit
 *
 ----------------------------------------------------------------------------*/
unsigned int getYoungRabbitAge(youngRabbit * rabbit, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief                   Get the age of the adult rabbit in input
 * 
 * @param rabbit            Pointer to the rabbit that you want the age of
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param posMatur          Index of the line in the table allRabbit->allAdult 
 *                          that contains the rabbit. If posMatur is equal 
 *                          to -1 the value will be calculated
 *
 * @return unsigned int     The age of the adult rabbit
 *
 ----------------------------------------------------------------------------*/
unsigned int getAdultRabbitAge(adultRabbit * rabbit, rabbitTable * allRabbit, 
    int posMatur);

/**----------------------------------------------------------------------------
 *
 * @brief                   Get the column index of an adult rabbit that has 
 *                          the same specifications that the ones in input
 * 
 * @param age               Age in month of the rabbit
 * @param ageMaturity       Age in month at which the rabbit as became an adult 
 * @param sex               Sex of the rabbit
 * @param numberOfLitter    Number of litter that the rabbit had had this year
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 *
 * @return unsigned int     Index of the column of the rabbit you want
 *
 ----------------------------------------------------------------------------*/
unsigned int getPosRabbit(unsigned int age, unsigned int ageMaturity, Sex sex, 
    unsigned int numberOfLitter, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief               Print a young rabbit into the FILE in input
 * 
 * @param file          File where you want to write the memory used to store
 *                      all the possible rabbits
 * @param rabbit        Pointer to the young rabbit that you want to print
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void printYoungRabbit(FILE * file, youngRabbit * rabbit, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief               Print an adult rabbit into the FILE in input
 * 
 * @param file          File where you want to write the memory used to store
 *                      all the possible rabbits
 * @param rabbit        Pointer to the adult rabbit that you want to print
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void printAdultRabbit(FILE * file, adultRabbit * rabbit, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief                   Create new rabbits and add them into the structure 
 *                          of all possible rabbits
 *
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param numberOfRabbit    Number of rabbit you wants to add
 *
 ----------------------------------------------------------------------------*/
void addNewRabbit(rabbitTable * allRabbit, unsigned int numberOfRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief                   Chek the total number of young rabbits that needs 
 *                          to be created from a certain number of litters 
 *                          and call addNewRabbit to create them
 * 
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param numberOfLitter    Number of litters
 *
 ----------------------------------------------------------------------------*/
void newRabbitLitter(rabbitTable * allRabbit, unsigned int numberOfLitter);

/**----------------------------------------------------------------------------
 *
 * @brief               Check the young rabbit in input to remove the rabbits 
 *                      that doesn't survive this month and move the rabbits 
 *                      to their new age
 * 
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 * @param rabbit        Rabbit that you want to check
 * @param rabbitPos     Index in allRabbit->allYoung of the rabbit you 
 *                      want to check
 *
 ----------------------------------------------------------------------------*/
void checkYoungRabbit(rabbitTable * allRabbit, youngRabbit rabbit, 
    unsigned int rabbitPos);

/**----------------------------------------------------------------------------
 *
 * @brief                   Check the adult rabbit in input to remove the 
 *                          rabbits that doesn't survive this month, move 
 *                          the rabbits to their new age and also check if 
 *                          females will get birth
 * 
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param rabbit            Rabbit that you want to check
 * @param rabbitPosMature   Line index in allRabbit->allAdult of the rabbit  
 *                          you want to check
 * @param rabbitPos         Column index in allRabbit->allAdult of the  
 *                          rabbit you want to check
 *
 ----------------------------------------------------------------------------*/
void checkAdultRabbit(rabbitTable * allRabbit, adultRabbit rabbit, 
    unsigned int rabbitPosMature, unsigned int rabbitPos);

/**----------------------------------------------------------------------------
 *
 * @brief               Check all the rabbits inside of allRabbit
 * 
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void checkAllRabbit(rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief                       Create or update the statistics of all the 
 *                              rabbit contained in allRabbit
 * 
 * @param stat                  Pointer to the structure of statistics of 
 *                              rabbits. If the pointer is NULL the structure 
 *                              will be created before being updated
 * @param allRabbit             Pointer to a structure that contains all 
 *                              the possible rabbits
 *
 * @return rabbitStatistic*     Pointer to the structure of statistics of 
 *                              rabbits
 *
 ----------------------------------------------------------------------------*/
rabbitStatistic * getStatistics(rabbitStatistic * stat, rabbitTable * allRabbit);

/**----------------------------------------------------------------------------
 *
 * @brief           Free all the statistics of all rabbits
 * 
 * @param stat      Address of a pointer to the rabbit statistics
 *
 ------------------------------------------------------------------------------*/
void freeRabbitStatistics(rabbitStatistic ** stat);


/**----------------------------------------------------------------------------
 * 
 * @brief               Run the simulation for the duration of months in input
 *                      and initialize it with nbYoungInit young rabbits. Once
 *                      the simulation has ended the stats are updated
 * 
 * @param duration      Duration in month that the simulation needs to run
 * @param nbYoungInit   Number of young rabbits the simulation needs to be
 *                      initialized with
 * @param allRabbit     Pointer to the structure of all the rabbits
 * @param stats         Pointer to the structure of the statistics
 * 
 ----------------------------------------------------------------------------*/
void runAllRabbitSimu(int duration, unsigned int nbYoungInit, 
    rabbitTable * allRabbit, rabbitStatistic * stats);

#endif