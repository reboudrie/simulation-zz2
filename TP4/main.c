#include <stdio.h>
#include <locale.h>

#include "Tp4.h"
#include "fiboRabbit.h"

int main(int argc, char * argv[]){
    (void) argv;
    (void) argc;
    setlocale(LC_NUMERIC, "");

    unsigned int nbStartSimu = 2;
    unsigned int nbEndSimu = 1002;
    int nbMonthStart = 1*12;
    int nbMonthEnd = 21*12;

    fprintf(stdout, "Rabbit Start,");
    for(int j=nbMonthStart; j<nbMonthEnd; j+=12){
        fprintf(stdout, "Year %d", j/12);
        if(j < nbMonthEnd-12){
            fprintf(stdout, ",");
        }
    }
    fprintf(stdout, "\n");

    // Run all the simulations for the simple rabbit simulation
    /*
    for(unsigned int i=nbStartSimu; i<nbEndSimu; i+=2){

        fprintf(stdout, "%u,", i);
        for(int j=nbMonthStart; j<nbMonthEnd; j+=12){
            long long unsigned int nbYoungRabbit = i;
            long long unsigned int nbAdultRabbit = 0;
            fprintf(stderr, "Young: %'llu, Age: %d\n", nbYoungRabbit, j/12);

            simulateSimpleRabbit(j, &nbYoungRabbit, &nbAdultRabbit);

            fprintf(stdout, "%'llu", nbYoungRabbit + nbAdultRabbit);
            if(j < nbMonthEnd-12){
                fprintf(stdout, ",");
            }
        }
        fprintf(stdout, "\n");

    }*/

    // Run all the simulations for the advanced rabbit simulation
    
    initAllRabbitStat();
    rabbitTable * allRabbit = genAllRabbit();
    rabbitStatistic * stats = getStatistics(stats, allRabbit);

    for(unsigned int i=nbStartSimu; i<nbEndSimu; i+=2){

        fprintf(stdout, "%u,", i);
        for(int j=nbMonthStart; j<nbMonthEnd; j+=12){
            fprintf(stderr, "Young: %'u, Age: %d\n", i, j/12);

            runAllRabbitSimu(j, i, allRabbit, stats);

            fprintf(stdout, "%lu", stats->totalRabbit);
            if(j < nbMonthEnd-12){
                fprintf(stdout, ",");
            }
        }
        fprintf(stdout, "\n");

    }

    freeRabbitStatistics(&stats);
    freeAllRabbit(&allRabbit);
    freeAllRabbitStat();

    return 0;
}