#ifndef __FIBO_RABBIT_H__
#define __FIBO_RABBIT_H__


/**----------------------------------------------------------------------------
 * 
 * @brief                   Function that simulate simply the reproduction of
 *                          rabbits.
 * 
 * @param   nbGeneration    Number of Generations in month the simulation
 *                          needs to run
 * @param   nbYoungRabbit   Total number of young rabbit that is left at the 
 *                          end of the simulation
 * @param   nbAdultRabbit   Total number of adult rabbit that is left at the 
 *                          end of the simulation
 * 
 ----------------------------------------------------------------------------*/
void simulateSimpleRabbit(int nbGeneration, long long unsigned int * nbYoungRabbit, 
    long long unsigned int * nbAdultRabbit);

#endif