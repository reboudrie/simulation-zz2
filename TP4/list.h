#ifndef __LIST_LE_RIZ_H__
#define __LIST_LE_RIZ_H__

/*-------------------------------------------------------------------------

                            All functions for the .h

-------------------------------------------------------------------------*/

#define createStructList(type, name)\
typedef struct elem ## name ## List {\
    type val;\
    struct elem ## name ## List * prev;\
    struct elem ## name ## List * next;\
} elem ## name ## List ;\
\
typedef struct name ## List {\
    elem ## name ## List * head;\
    elem ## name ## List * end;\
    unsigned int size;\
} name ## List ;

#define createAllListDef(type, name)\
createStructList(type, name)\
\
name ## List  * init ## name ## List ();\
elem ## name ## List * createElem ## name ## List (type value);\
int isEmpty ## name ## List ( name ## List * list);\
int isHead ## name ## List (elem ## name ## List * elem);\
int isEnd ## name ## List (elem ## name ## List * elem);\
unsigned int getSize ## name ## List (name ## List * list);\
elem ## name ## List * getHeadElem ## name ## List (name ## List * list);\
elem ## name ## List * getPrevElem ## name ## List (elem ## name ## List * elem);\
elem ## name ## List ** getAddrPointerPrevElem ## name ## List(elem ## name ## List * elem);\
elem ## name ## List * getNextElem ## name ## List (elem ## name ## List * elem);\
elem ## name ## List ** getAddrPointerNextElem ## name ## List(elem ## name ## List * elem);\
elem ## name ## List * getEndElem ## name ## List (name ## List * list);\
elem ## name ## List * getPosElem ## name ## List (name ## List * list, unsigned int position);\
type * getValueElem ## name ## Liste (elem ## name ## List * elem);\
void prepend ## name ## List (name ## List * list, elem ## name ## List * elem);\
void addAt ## name ## List (name ## List * list, elem ## name ## List ** addrPos, elem ## name ## List * elem);\
void append ## name ## List ( name ## List * list, elem ## name ## List * elem);\
void delHead ## name ## List (name ## List * list);\
void delAddr ## name ## List (name ## List * list, elem ## name ## List ** addrPos);\
void delEnd ## name ## List (name ## List * list);\
void free ## name ## List (name ## List ** list);\
\
void freeElem ## name ## List (elem ## name ## List ** elem);

/*-------------------------------------------------------------------------

                            All functions for the .c

-------------------------------------------------------------------------*/

/*
    Be careful this macro does NOT implement all the functions needed !!!!
    You need to choose wich macro you want for free
*/
#define createAllDefaultListFunction(type, name) \
createInitList(type, name)\
createCreateElemList(type, name)\
createIsEmptyList(type, name)\
createIsHeadList(type, name)\
createIsEndList(type, name)\
createGetSizeList(type, name)\
createGetHeadElemList(type, name)\
createGetPrevElemList(type, name)\
createGetAddrPointerPrevElem(type, name)\
createGetNextElemList(type, name)\
createGetAddrPointerNextElem(type, name)\
createGetEndElemList(type, name)\
createGetPosElemList(type, name)\
createGetValueElemList(type, name)\
createPreppendList(type, name)\
createAddAtList(type, name)\
createAppendList(type, name)\
createHeadDelList(type, name)\
createAddrDelList(type, name)\
createEndDelList(type, name)\
createFreeList(type, name)


#define createInitList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                          Function that will create an empty list that can be     */\
/*                                  used, the list needs to be freed when you don't need it */\
/*                                  anymore.                                                */\
/*                                                                                          */\
/*  @return     name ## List *      Returns an object list that is empty                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
name ## List * init ## name ## List (){\
    name ## List * newList = calloc(1,sizeof(name ## List));\
    return newList;\
}

#define createCreateElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that will create an element for a list */\
/*                                          that has a value of "value", this element needs */\
/*                                          to be freed.                                    */\
/*                                                                                          */\
/*  @param      value                       The value that will be put in the element that  */\
/*                                          will be created                                 */\
/*  @return     elem ## name ## List *      Returns an object that is the element of a list */\
/*                                          containing the value "value"                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * createElem ## name ## List (type value){\
    elem ## name ## List * elem = calloc(1, sizeof(elem ## name ## List));\
    elem->val = value;\
    return elem;\
}

#define createIsEmptyList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief              Function that will return 1 if the list in input is empty and 0 if  */\
/*                      it's not.                                                           */\
/*                                                                                          */\
/*  @param      list    Pointer to the list that we need to check if it's empty or not      */\
/*  @return     int     Returns if the list is empty or not                                 */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
int isEmpty ## name ## List (name ## List * list){\
    return !list->size;\
}

#define createIsHeadList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief              Function that will return 1 if the element of list in input is the  */\
/*                      head of a list and 0 if it's not.                                   */\
/*                                                                                          */\
/*  @param      elem    Pointer to the element that needs to check if it's at the beginning */\
/*                      of a list                                                           */\
/*  @return     int     Returns if the element is the head or not                           */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
int isHead ## name ## List (elem ## name ## List * elem){\
    return !elem->prev;\
}

#define createIsEndList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief              Function that will return 1 if the element of list in input is the  */\
/*                      end of a list and 0 if it's not.                                    */\
/*                                                                                          */\
/*  @param      elem    Pointer to the element that needs to check if it's at the end       */\
/*                      of a list                                                           */\
/*  @return     int     Returns if the element is the end or not                            */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
int isEnd ## name ## List (elem ## name ## List * elem){\
    return !elem->next;\
}

#define createGetSizeList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                      Function that returns the size of the list that is in input.*/\
/*                                                                                          */\
/*  @param      list            List that you needs to get the size of                      */\
/*  @return     unsigned int    Returns the size of the list                                */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
unsigned int getSize ## name ## List (name ## List * list){\
    return list->size;\
}

#define createGetHeadElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the element that is at the  */\
/*                                          beginning of the list in input.                 */\
/*                                                                                          */\
/*  @param      list                        List that you needs to get the head of          */\
/*  @return     elem ## name ## List *      Returns the element that is at the beginning    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * getHeadElem ## name ## List (name ## List * list){\
    return list->head;\
}

#define createGetPrevElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the previous element of the */\
/*                                          list of the element in input, if the element is */\
/*                                          at the beginning of the list it will return     */\
/*                                          NULL.                                           */\
/*                                                                                          */\
/*  @param      elem                        Element that you want the previous element of   */\
/*                                          the list                                        */\
/*  @return     elem ## name ## List *      Returns the previous element                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * getPrevElem ## name ## List (elem ## name ## List * elem){\
    return elem->prev;\
}

#define createGetAddrPointerPrevElem(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the address of the pointer  */\
/*                                          to the previous element. This function will be  */\
/*                                          mainly useful for addAt ## name ## List and     */\
/*                                          delAddr ## name ## List.                        */\
/*                                                                                          */\
/*  @param      elem                        Element that you want the address of the pointer*/\
/*                                          to the previous element                         */\
/*  @return     elem ## name ## List **     Returns the address of the pointer to the       */\
/*                                          previous element                                */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List ** getAddrPointerPrevElem ## name ## List(elem ## name ## List * elem){\
    return &elem->prev;\
}

#define createGetNextElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the next element of the     */\
/*                                          list of the element in input, if the element is */\
/*                                          at the end of the list it will return NULL.     */\
/*                                                                                          */\
/*  @param      elem                        Element that you want the next element of the   */\
/*                                          list                                            */\
/*  @return     elem ## name ## List *      Returns the next element                        */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * getNextElem ## name ## List (elem ## name ## List * elem){\
    return elem->next;\
}

#define createGetAddrPointerNextElem(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the address of the pointer  */\
/*                                          to the next element. This function will be      */\
/*                                          mainly useful for addAt ## name ## List and     */\
/*                                          delAddr ## name ## List.                        */\
/*                                                                                          */\
/*  @param      elem                        Element that you want the address of the pointer*/\
/*                                          to the next element                             */\
/*  @return     elem ## name ## List **     Returns the address of the pointer to the       */\
/*                                          next element                                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List ** getAddrPointerNextElem ## name ## List(elem ## name ## List * elem){\
    return &elem->next;\
}

#define createGetEndElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the element that is at the  */\
/*                                          end of the list in input.                       */\
/*                                                                                          */\
/*  @param      list                        List that you needs to get the end of           */\
/*  @return     elem ## name ## List *      Returns the element that is at the end          */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * getEndElem ## name ## List (name ## List * list){\
    return list->end;\
}

#define createGetPosElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                                  Function that gives the element that is at the  */\
/*                                          position given in input, if the position is not */\
/*                                          inside the list it will return NULL. The element*/\
/*                                          is researched by looking if it's closer to the  */\
/*                                          beginning or the end of the list and using the  */\
/*                                          shortest method.                                */\
/*                                                                                          */\
/*  @param      list                        List that you needs to get the element of       */\
/*  @param      position                    Position of the element that you want to have   */\
/*  @return     elem ## name ## List *      Returns the element that is at the position     */\
/*                                          given                                           */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
elem ## name ## List * getPosElem ## name ## List (name ## List * list, unsigned int position){\
    elem ## name ## List * pos = NULL;\
    if(position < list->size/2){\
        pos = list->head;\
        for(unsigned int i=0; i<position; i++){\
            pos = pos->next;\
        }\
    } else if(position < list->size){\
        pos = list->end;\
        for(unsigned int i=list->size-1; i>position; i--){\
            pos = pos->prev;\
        }\
    }\
    return pos;\
}

#define createGetValueElemList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that gives the value stored in the element given in    */\
/*                          input.                                                          */\
/*                                                                                          */\
/*  @param      elem        Element that you want the value of                              */\
/*  @return     type        Returns the value of the element                                */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
type * getValueElem ## name ## Liste (elem ## name ## List * elem){\
    return &elem->val;\
}

#define createPreppendList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will add the element in input at the beginning of */\
/*                          the list in input.                                              */\
/*                                                                                          */\
/*  @param      list        List that you want to add the element at the beginning          */\
/*  @param      elem        Element that you want to add                                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void prepend ## name ## List (name ## List * list, elem ## name ## List * elem){\
    elem->prev = NULL;\
    elem->next = list->head;\
    if(!list->end){\
        list->end = list->head;\
    } else {\
        list->head->prev = elem;\
    }\
    list->head = elem;\
    list->size++;\
}

#define createAddAtList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will add an element in the list between the       */\
/*                          element at the adress pointed at in input and the element at    */\
/*                          the address addrPos. If the adress in input is at the beginning */\
/*                          or the end it will run the appropriate function.                */\
/*                                                                                          */\
/*  @param      list        List that you want to add an element in                         */\
/*  @param      addrPos     Pointer to the adress that you want to change to add the new    */\
/*                          element                                                         */\
/*  @param      elem        Element that you want to add in the list                        */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void addAt ## name ## List (name ## List * list, elem ## name ## List ** addrPos, elem ## name ## List * elem){\
    /*Check if the position to add the element is at the beginning of the list*/\
    if(addrPos == &list->head || (*addrPos != NULL && addrPos == &list->head->prev)){\
        prepend ## name ## List (list, elem);\
    /*Check if the position to add the element is at the end of the list*/\
    } else if(addrPos == &list->end || (*addrPos != NULL && addrPos == &list->end->next)){\
        append ## name ## List (list, elem);\
    } else {\
        /*Check if address of addrPos correspond at a next of an element*/\
        if(addrPos == &((*addrPos)->prev->next)){\
            elem->next = *addrPos;\
            elem->prev = (*addrPos)->prev;\
            (*addrPos)->prev = elem;\
        } else {\
            elem->prev = *addrPos;\
            elem->next = (*addrPos)->next;\
            (*addrPos)->next = elem;\
        }\
        *addrPos = elem;\
        list->size++;\
    }\
}

#define createAppendList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will add the element in input at the end of the   */\
/*                          list in input.                                                  */\
/*                                                                                          */\
/*  @param      list        List that you want to add the element at the end                */\
/*  @param      elem        Element that you want to add                                    */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void append ## name ## List (name ## List * list, elem ## name ## List * elem){\
    elem->next = NULL;\
    elem->prev = list->end;\
    if(list->end){\
        list->end->next = elem;\
    }else{\
        list->head = elem;\
    }\
    list->end = elem;\
    list->size++;\
}

#define createHeadDelList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will remove the element at the beginning of the   */\
/*                          list in input.                                                          */\
/*                                                                                          */\
/*  @param      list        List that you want to remove the element at the beginning       */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void delHead ## name ## List (name ## List * list){\
    elem ## name ## List * elemToDel = list->head;\
    list->head = elemToDel->next;\
    if(list->end == elemToDel){\
        list->end = NULL;\
    } else {\
        list->head->prev = NULL;\
    }\
    freeElem ## name ## List(&elemToDel);\
    list->size--;\
}

#define createAddrDelList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will delete an element in the list at the adress  */\
/*                          pointed at in input. If the adress in input is at the beginning */\
/*                          or the end it will run the appropriate function.                */\
/*                                                                                          */\
/*  @param      list        List that you want to delete an element of                      */\
/*  @param      addrPos     Pointer to the adress that you want to change to delete the     */\
/*                          element                                                         */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void delAddr ## name ## List (name ## List * list, elem ## name ## List ** addrPos){\
    if(*addrPos == list->head){\
        delHead ## name ## List (list);\
    } else if(*addrPos == list->end){ \
        delEnd ## name ## List (list);\
    } else {\
        elem ## name ## List * elemToDel = *addrPos;\
        if(addrPos == &(elemToDel->prev->next)){\
            elemToDel->next->prev = elemToDel->prev;\
            *addrPos = elemToDel->next;\
        } else {\
            elemToDel->prev->next = elemToDel->next;\
            *addrPos = elemToDel->prev;\
            }\
        freeElem ## name ## List(&elemToDel);\
        list->size--;\
    }\
}

#define createEndDelList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will remove the element at the end of the list in */\
/*                          input.                                                          */\
/*                                                                                          */\
/*  @param      list        List that you want to remove the element at the end             */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void delEnd ## name ## List (name ## List * list){\
    elem ## name ## List * elemToDel = list->end;\
    if(list->head == elemToDel){\
        list->head = NULL;\
    } else {\
        elemToDel->prev->next = NULL;\
    }\
    list->end = elemToDel->prev;\
    freeElem ## name ## List(&elemToDel);\
    list->size--;\
}

#define createFreeList(type, name) \
/**-----------------------------------------------------------------------------------------*/\
/*                                                                                          */\
/*  @brief                  Function that will go through the list and free each element    */\
/*                          of it and the value inside each element.                        */\
/*                                                                                          */\
/*  @param      list        Adress of the pointer to the lis that you want to free          */\
/*                                                                                          */\
/*------------------------------------------------------------------------------------------*/\
void free ## name ## List (name ## List ** list){ \
    elem ## name ## List ** pos = &((*list)->head);\
    while(*pos){\
        elem ## name ## List ** elemToFree = pos;\
        pos = &((*pos)->next);\
        freeElem ## name ## List (elemToFree);\
    }\
    free(*list);\
    *list = NULL;\
}

/*------------------------------------------------------------------------

                Begining of Functions that needs to special inputs

------------------------------------------------------------------------*/

/*------------------------------------------------------------------------

                                Function Free

------------------------------------------------------------------------*/

// This free is used when you don't need to free the value that you put
// in your list
#define createFreeElemListBasic(type, name) \
void freeElem ## name ## List (elem ## name ## List ** elem){\
    free(*elem);\
    *elem = NULL;\
}

// This free needs to be used when you have a value in your list that
// needs to be freed
#define createFreeElemListWithFunc(type, name, funcName) \
void freeElem ## name ## List (elem ## name ## List ** elem){\
    funcName((*elem)->val);\
    free(*elem);\
    *elem = NULL;\
}

#endif