#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Tp4.h"
#include "mt19937ar.h"

double probaSexMale;
double probaSurvYoung;
double probaSurvAdult;
unsigned int ageStartSurvDecrease;
double probaSurvDecrease;
unsigned int spacingSurvDecrease;
double * probaSurv;
unsigned int maxSurvAge;
unsigned int ageMaturityStart;
unsigned int numberMonthMaturity;
unsigned int femaleNumberLitterPerYear[2];
unsigned int numberBabyPerLitter[2];
double * probaNumberBaby;


/**----------------------------------------------------------------------------
 *
 * @brief       Initialise all the global variables for the rabbits
 * 
 ----------------------------------------------------------------------------*/
void initAllRabbitStat(){

    probaSexMale = 0.5;

    double probaSurvYoungYear = 0.35;
    probaSurvYoung = pow(probaSurvYoungYear, 1.0 / 12.);

    double probaSurvAdultYear = 0.60;
    probaSurvAdult = pow(probaSurvAdultYear, 1.0 / 12.);

    ageStartSurvDecrease = 10 * 12; // 10 years
    probaSurvDecrease = 0.1;
    spacingSurvDecrease = 12;       // Every year
    maxSurvAge = 15 * 12;           // 15 years
    
    probaSurv = calloc((maxSurvAge-ageStartSurvDecrease)/spacingSurvDecrease, sizeof(double));
    for(unsigned int i = 0; i<(maxSurvAge-ageStartSurvDecrease)/spacingSurvDecrease; i++){
        probaSurv[i] = pow(probaSurvAdultYear-(probaSurvDecrease)*(i+1), 1.0 / 12.);
    }

    ageMaturityStart = 5;
    numberMonthMaturity = 3;

    femaleNumberLitterPerYear[0] = 3;
    femaleNumberLitterPerYear[1] = 9;

    numberBabyPerLitter[0] = 3;
    numberBabyPerLitter[1] = 6;

    unsigned int sizeProbaNumberBaby = numberBabyPerLitter[1] - numberBabyPerLitter[0] + 1;
    probaNumberBaby = calloc(sizeProbaNumberBaby, sizeof(double));
    for(unsigned int i=0; i<sizeProbaNumberBaby; i++){
        probaNumberBaby[i] = ((double) i+1)/(double) sizeProbaNumberBaby;
    }
}


/**----------------------------------------------------------------------------
 *
 * @brief       Free all the global variables for the rabbits
 * 
 ----------------------------------------------------------------------------*/
void freeAllRabbitStat(){
    free(probaSurv);
    free(probaNumberBaby);
}


/**----------------------------------------------------------------------------
 * 
 * @brief                   This function will make nbDraw check with each one 
 *                          of a probability of proba and return the number 
 *                          of successful draws
 * 
 * @param nbDraw            Number of draws that needs to be made
 * @param proba             Probability of success
 * 
 * @return unsigned int     Number of successful draws
 * 
 ----------------------------------------------------------------------------*/
unsigned int nDraw(unsigned int nbDraw, double proba){
    unsigned int success = 0;

    for(unsigned int i=0; i<nbDraw; i++){
        if(genrand_real1() <= proba){
            success++;
        }
    }

    return success;
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Create and initialise every rabbit possible and
 *                          returns it
 * 
 * @return rabbitTable*     Returns a pointer to a structure that contains 
 *                          every rabbit possible 
 *
 ----------------------------------------------------------------------------*/
rabbitTable * genAllRabbit(){

    rabbitTable * allRabbit = calloc(1, sizeof(rabbitTable));
    allRabbit->sizeYoung = (ageMaturityStart+numberMonthMaturity)*2;
    allRabbit->sizeMature = numberMonthMaturity+1;
    allRabbit->sizeAdult = calloc(allRabbit->sizeMature, sizeof(unsigned int));
    allRabbit->allYoung = calloc(allRabbit->sizeYoung, sizeof(youngRabbit));
    allRabbit->allAdult = calloc(allRabbit->sizeMature, sizeof(adultRabbit *));
    allRabbit->sizeAdultYear = calloc(13, sizeof(unsigned int));
    unsigned int sizeAdult = 0;

    // We go through each line of the table allRabbit->allAdult
    for(unsigned int i=0; i<allRabbit->sizeMature; i++){

        int maxSurvAgeLastYear = (maxSurvAge-ageMaturityStart-i)%12;
        int maxSurvAgeYear = (maxSurvAge-ageMaturityStart-i)/12;
        int maxLitter = femaleNumberLitterPerYear[1];

        // We calculate the size of this row of the table of all adult rabbits 
        // by first calculating the size needed for the number of years and then 
        // adding the size needed for the remaining months until the maximum age a 
        // rabbit can be
        allRabbit->sizeAdult[i] = maxSurvAgeYear * 
            (12*2 + (12-maxLitter)*maxLitter + (maxLitter*(maxLitter+1))/2);
        
        if(maxSurvAgeLastYear <= maxLitter){
            allRabbit->sizeAdult[i] += maxSurvAgeLastYear*2 + 
                (maxSurvAgeLastYear*(maxSurvAgeLastYear+1))/2;
        }else{
            allRabbit->sizeAdult[i] += maxSurvAgeLastYear*2 + 
                (maxSurvAgeLastYear-maxLitter)*maxLitter + (maxLitter*(maxLitter+1))/2;
        }
        sizeAdult += allRabbit->sizeAdult[i];

        allRabbit->allAdult[i] = calloc(allRabbit->sizeAdult[i], sizeof(adultRabbit));
        int rabbitTypePos = -1, curMaxNbLitter = 1;

        // We go through all the cells in this row of the table of all the adult 
        // rabbits and fill it with all the possible values
        for(unsigned int j=0; j<allRabbit->sizeAdult[i]; j++){
            int yearPos = j%(12*2 + (12-maxLitter)*maxLitter + (maxLitter*(maxLitter+1))/2);
            allRabbit->allAdult[i][j].numberOfRabbit = 0;
            
            if(!yearPos){
                rabbitTypePos = -1;
                curMaxNbLitter = 1;
            }

            if(rabbitTypePos == -1){
                allRabbit->allAdult[i][j].sex = Male;
                allRabbit->allAdult[i][j].numberOfLitter = 0;
            }else{
                allRabbit->allAdult[i][j].sex = Female;
                allRabbit->allAdult[i][j].numberOfLitter = rabbitTypePos;
            }

            if(rabbitTypePos == curMaxNbLitter){
                rabbitTypePos = -1;
                if(curMaxNbLitter < maxLitter){
                    curMaxNbLitter++;
                }
            }else{
                rabbitTypePos++;
            }
        }
    }

    // We go through the table of all the young rabbits and fill it with the value of sex
    for(unsigned int i=0; i<allRabbit->sizeYoung-1; i+=2){
        allRabbit->allYoung[i].sex = Male;
        allRabbit->allYoung[i].numberOfRabbit = 0;

        allRabbit->allYoung[i+1].sex = Female;
        allRabbit->allYoung[i+1].numberOfRabbit = 0;
    }

    // We calculate all the values of the number of cells it would take to store i months
    // of adult rabbits
    for(unsigned int i=1; i<13; i++){
        if(i <= femaleNumberLitterPerYear[1]){
            allRabbit->sizeAdultYear[i] = 2*i + (i*(i+1))/2;
        }else{
            allRabbit->sizeAdultYear[i] = 2*i + 
            (i-femaleNumberLitterPerYear[1])*femaleNumberLitterPerYear[1] + 
            (femaleNumberLitterPerYear[1]*(femaleNumberLitterPerYear[1]+1))/2;
        }
    }

    allRabbit->totalAdultRabbit = sizeAdult;

    return allRabbit;
}


/**----------------------------------------------------------------------------
 *
 * @brief               Free the structure of every rabbit possible
 * 
 * @param allRabbit     Address of a pointer that contains a structure of 
 *                      every rabbit possible  
 *
 ----------------------------------------------------------------------------*/
void freeAllRabbit(rabbitTable ** allRabbit){

    for(unsigned int i=0; i<(*allRabbit)->sizeMature; i++){
        free((*allRabbit)->allAdult[i]);
    }

    free((*allRabbit)->sizeAdult);
    free((*allRabbit)->allYoung);
    free((*allRabbit)->allAdult);
    free((*allRabbit)->sizeAdultYear);
    free(*allRabbit);
    *allRabbit = NULL;
}


/**----------------------------------------------------------------------------
 *
 * @brief               Print the memory that is used to store all the 
 *                      possible rabbits
 * 
 * @param file          File where you want to write the memory used to 
 *                      store all the possible rabbits
 * @param allRabbit     Pointer to a structure that contains all the 
 *                      possible rabbits
 *
 ----------------------------------------------------------------------------*/
void printRabbitMemory(FILE * file, rabbitTable * allRabbit){
    fprintf(file, "There is %'u young rabbits which takes %'luo\
            and %'u adult rabbits which takes %'luo. For a grand total of \
            %'luo.\n\n", allRabbit->sizeYoung, allRabbit->sizeYoung*sizeof(youngRabbit), 
            allRabbit->totalAdultRabbit, allRabbit->totalAdultRabbit*sizeof(adultRabbit), 
            allRabbit->totalAdultRabbit*sizeof(adultRabbit) + 
            allRabbit->sizeYoung*sizeof(youngRabbit));
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Get the age of the young rabbit in input
 * 
 * @param rabbit            Pointer to the rabbit that you want the age of
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 *
 * @return unsigned int     The age of the young rabbit
 *
 ----------------------------------------------------------------------------*/
unsigned int getYoungRabbitAge(youngRabbit * rabbit, rabbitTable * allRabbit){
    unsigned int age = 0;

    // Check if the rabbit is inside the the table of all the rabbit
    if(rabbit > allRabbit->allYoung && rabbit < &(allRabbit->allYoung[allRabbit->sizeYoung])){
        age = (rabbit - allRabbit->allYoung)/2 + 1;
    }

    return age;
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Get the age of the adult rabbit in input
 * 
 * @param rabbit            Pointer to the rabbit that you want the age of
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param posMatur          Index of the line in the table allRabbit->allAdult 
 *                          that contains the rabbit. If posMatur is equal 
 *                          to -1 the value will be calculated
 *
 * @return unsigned int     The age of the adult rabbit
 *
 ----------------------------------------------------------------------------*/
unsigned int getAdultRabbitAge(adultRabbit * rabbit, 
    rabbitTable * allRabbit, int posMatur){
    
    unsigned int age = 0;
    unsigned int posMaturLocal = 0;

    // Check if we don't know the first position in allRabbit->allAdult and 
    // calculate it if we don't
    if(posMatur < 0){

        // We go through the line index and try to find the actual position
        // of the line index of the rabbit in input
        while(posMaturLocal < allRabbit->sizeMature && 
              !(&(allRabbit->allAdult[posMaturLocal][0]) < rabbit && 
              rabbit < &(allRabbit->allAdult[posMaturLocal][allRabbit->sizeAdult[posMaturLocal]-1])+1)){
            
            posMaturLocal++;
        }
    }else{

        posMaturLocal = posMatur;
    }

    // Check if the line index in input or found is not out of the table
    if(posMaturLocal < allRabbit->sizeMature){
        age = posMaturLocal + ageMaturityStart;
        unsigned int sizeYear = allRabbit->sizeAdultYear[12];
        unsigned int posTab = rabbit - &(allRabbit->allAdult[posMaturLocal][0]);
        unsigned int ageYear = posTab/sizeYear;
        unsigned int posYear = posTab%sizeYear;

        // Try to find the month of the year that correspond to the rabbit age
        unsigned int monthPos = 0;
        while(monthPos < 13 && 
              !(posYear >= allRabbit->sizeAdultYear[monthPos] && 
              posYear <allRabbit->sizeAdultYear[monthPos+1])){
            
            monthPos++;
        }


        age += 12*ageYear + monthPos;
    }

    return age;
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Get the column index of an adult rabbit that has 
 *                          the same specifications that the ones in input
 * 
 * @param age               Age in month of the rabbit
 * @param ageMaturity       Age in month at which the rabbit as became an adult 
 * @param sex               Sex of the rabbit
 * @param numberOfLitter    Number of litter that the rabbit had had this year
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 *
 * @return unsigned int     Index of the column of the rabbit you want
 *
 ----------------------------------------------------------------------------*/
unsigned int getPosRabbit(unsigned int age, unsigned int ageMaturity, Sex sex, 
    unsigned int numberOfLitter, rabbitTable * allRabbit){
    
    // Get the the number of table cell that will be used for a year
    unsigned int sizeYear = allRabbit->sizeAdultYear[12];

    // Get the position of the rabbit by first getting the year age of the 
    // rabbit and multiplying it by the size of a year
    unsigned int pos = ((age-ageMaturity)/12)*sizeYear;
    // then we add the number of table cell that would be needed for a number
    // of month that correspond to the age in month modulo 12 of the rabbit
    pos += allRabbit->sizeAdultYear[(age-ageMaturity)%12];
    
    if(sex == Female){
        pos += numberOfLitter + 1;
    }

    return pos;
}


/**----------------------------------------------------------------------------
 *
 * @brief               Print a young rabbit into the FILE in input
 * 
 * @param file          File where you want to write the memory used to store
 *                      all the possible rabbits
 * @param rabbit        Pointer to the young rabbit that you want to print
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void printYoungRabbit(FILE * file, youngRabbit * rabbit, rabbitTable * allRabbit){

    fprintf(file, "%u rabbit ", rabbit->numberOfRabbit);

    if(rabbit->sex){
        fprintf(file, "female ");
    }else{
        fprintf(file, "male ");
    }

    fprintf(file, "of %u month old\n", getYoungRabbitAge(rabbit, allRabbit));
}


/**----------------------------------------------------------------------------
 *
 * @brief               Print an adult rabbit into the FILE in input
 * 
 * @param file          File where you want to write the memory used to store
 *                      all the possible rabbits
 * @param rabbit        Pointer to the adult rabbit that you want to print
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void printAdultRabbit(FILE * file, adultRabbit * rabbit, rabbitTable * allRabbit){

    fprintf(file, "%'u rabbit ", rabbit->numberOfRabbit);

    if(rabbit->sex){
        fprintf(file, "female that already had %u litters this year ", rabbit->numberOfLitter);
    }else{
        fprintf(file, "male ");
    }

    unsigned int age = getAdultRabbitAge(rabbit, allRabbit, -1);
    fprintf(file, "of %u years and %u months old ", age/12, age%12);

    if(rabbit->sex){
        fprintf(file, "that already had %u litters this year\n", rabbit->numberOfLitter);
    }else{
        fprintf(file, "\n");
    }
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Create new rabbits and add them into the structure 
 *                          of all possible rabbits
 *
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param numberOfRabbit    Number of rabbit you wants to add
 *
 ----------------------------------------------------------------------------*/
void addNewRabbit(rabbitTable * allRabbit, unsigned int numberOfRabbit){

    // Get the number of rabbits that will be males
    unsigned int numberMale = nDraw(numberOfRabbit, probaSexMale);

    // Add the number of males and females to the table of all young rabbits
    allRabbit->allYoung[0].numberOfRabbit += numberMale;
    allRabbit->allYoung[1].numberOfRabbit += numberOfRabbit-numberMale;
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Chek the total number of young rabbits that needs 
 *                          to be created from a certain number of litters 
 *                          and call addNewRabbit to create them
 * 
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param numberOfLitter    Number of litters
 *
 ----------------------------------------------------------------------------*/
void newRabbitLitter(rabbitTable * allRabbit, unsigned int numberOfLitter){
    unsigned int totalNewRabbit = 0;

    // Loop to check for each litter how many babies will be born
    for(unsigned int i=0; i<numberOfLitter; i++){
        double probaRabbitToGet = genrand_real1();
        unsigned int nbRabbitToCreate = numberBabyPerLitter[0];
    
        while(probaRabbitToGet > probaNumberBaby[(int) (nbRabbitToCreate-numberBabyPerLitter[0])] 
              && nbRabbitToCreate < numberBabyPerLitter[1] - numberBabyPerLitter[0]){
    
            nbRabbitToCreate++;
        }

        // Add the number of rabbits that will be born to the total
        totalNewRabbit += nbRabbitToCreate;
    }

    // Call the function to add the calculated number of new rabbits to the table
    addNewRabbit(allRabbit, totalNewRabbit);
}


/**----------------------------------------------------------------------------
 *
 * @brief               Check the young rabbit in input to remove the rabbits 
 *                      that doesn't survive this month and move the rabbits 
 *                      to their new age
 * 
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 * @param rabbit        Rabbit that you want to check
 * @param rabbitPos     Index in allRabbit->allYoung of the rabbit you 
 *                      want to check
 *
 ----------------------------------------------------------------------------*/
void checkYoungRabbit(rabbitTable * allRabbit, youngRabbit rabbit, 
    unsigned int rabbitPos){

    // Check if there is at least one rabbit to check
    if(rabbit.numberOfRabbit != 0){

        // Get the number of rabbits that will survive
        unsigned int nbSurvive = nDraw(rabbit.numberOfRabbit, probaSurvYoung);

        // Check that the rabbit is between the ages at which we can become adults
        if(rabbitPos < (ageMaturityStart+numberMonthMaturity-1)*2 && 
           rabbitPos >= (ageMaturityStart-1)*2){

            // Determine the new position these rabbits will have in the adult rabbit table
            int posAdult = ((rabbitPos+2)/2)-ageMaturityStart;

            // Get the number of rabbits that will become adults this month
            double probaAdult = 1./(double)(1+numberMonthMaturity);
            unsigned int nbAdult = nDraw(nbSurvive, probaAdult);

            // Remove the number of adult rabbits from the number of young rabbits 
            // that has survived 
            nbSurvive -= nbAdult;

            // Check the sex of the rabbit
            if(rabbit.sex == Male){

                allRabbit->allAdult[posAdult][0].numberOfRabbit = nbAdult;
            }else{

                // Get the number of female rabbits that will have a litter
                double probaLitter = ((double)femaleNumberLitterPerYear[0])/12.;
                unsigned int nbLitter = nDraw(nbAdult, probaLitter);

                // Get the number of new rabbits that will be added and add them 
                newRabbitLitter(allRabbit, nbLitter);

                // Place the number of females that had a litter and those that 
                // didn't in their respective locations 
                allRabbit->allAdult[posAdult][1].numberOfRabbit = 
                    nbAdult - nbLitter;
                allRabbit->allAdult[posAdult][2].numberOfRabbit = nbLitter;
            }
        }
        
        // Check that the rabbits are in the last position of the young rabbit trable,
        // as this means that they will automatically become adults
        if(rabbitPos == allRabbit->sizeYoung-1 || rabbitPos == allRabbit->sizeYoung-2){

            // Determine the new position these rabbits will have in the adult rabbit table
            int posAdult = (rabbitPos/2)-ageMaturityStart;

            // Check the sex of the rabbit
            if(rabbit.sex == Male){
                allRabbit->allAdult[posAdult][0].numberOfRabbit = nbSurvive;
            }else{

                // Get the number of female rabbits that will have a litter
                double probaLitter = ((double)femaleNumberLitterPerYear[0])/12.;
                unsigned int nbLitter = nDraw(nbSurvive, probaLitter);

                // Get the number of new rabbits that will be added and add them 
                newRabbitLitter(allRabbit, nbLitter);

                // Place the number of females that had a litter and those that 
                // didn't in their respective locations 
                allRabbit->allAdult[posAdult][1].numberOfRabbit = nbSurvive - nbLitter;
                allRabbit->allAdult[posAdult][2].numberOfRabbit = nbLitter;
            }
        }else{
            // Put the remaining of rabbits at the next position in the table of 
            // all young rabbits
            allRabbit->allYoung[rabbitPos+2].numberOfRabbit = nbSurvive;

            // Make sure don't check the month-old rabbit because if we are that 
            // means that the values that we have are saved and so we don't want 
            // to reset the actual values that are in the table of all the young
            // rabbits
            if(rabbitPos != 0 && rabbitPos != 1){
                allRabbit->allYoung[rabbitPos].numberOfRabbit = 0;
            }
        }
    }
}


/**----------------------------------------------------------------------------
 *
 * @brief                   Check the adult rabbit in input to remove the 
 *                          rabbits that doesn't survive this month, move 
 *                          the rabbits to their new age and also check if 
 *                          females will get birth
 * 
 * @param allRabbit         Pointer to a structure that contains all the 
 *                          possible rabbits
 * @param rabbit            Rabbit that you want to check
 * @param rabbitPosMature   Line index in allRabbit->allAdult of the rabbit  
 *                          you want to check
 * @param rabbitPos         Column index in allRabbit->allAdult of the  
 *                          rabbit you want to check
 *
 ----------------------------------------------------------------------------*/
void checkAdultRabbit(rabbitTable * allRabbit, adultRabbit rabbit, 
    unsigned int rabbitPosMature, unsigned int rabbitPos){
    
    // Check if there is at least one rabbit to check
    if(rabbit.numberOfRabbit != 0){
        // Get the age of the adult rabbit from the position in the table
        unsigned int age = getAdultRabbitAge(
            &(allRabbit->allAdult[rabbitPosMature][rabbitPos]), allRabbit, 
            rabbitPosMature);
        
        // Check if the rabbit is not at his maximal age
        if(age < maxSurvAge-1){

            unsigned int nbSurvive = 0;
            // Get the position in the table for the rabbit that will be one 
            // month older
            unsigned int newPos = getPosRabbit(age+1, rabbitPosMature+ageMaturityStart, 
                rabbit.sex, rabbit.numberOfLitter, allRabbit);

            // Get the number of rabbits that will survive depending if the rabbit is
            // older than the age of survival decrease
            if(age < ageStartSurvDecrease){
                nbSurvive = nDraw(rabbit.numberOfRabbit, probaSurvYoung);
            }else{
                nbSurvive = nDraw(rabbit.numberOfRabbit, 
                    probaSurv[(age-ageStartSurvDecrease)/spacingSurvDecrease]);
            }

            // Check if the rabbit can have a new litter
            if((rabbit.sex == Female && (age-rabbitPosMature-ageMaturityStart)%12 == 11) || 
               (rabbit.sex == Female && rabbit.numberOfLitter < femaleNumberLitterPerYear[1])){

                // Change the position of the new rabbit because the number of litters
                // resets every year
                if((age-rabbitPosMature-ageMaturityStart)%12 == 11){
                    newPos = getPosRabbit(age+1, rabbitPosMature+ageMaturityStart, 
                                          Female, 0, allRabbit);
                }

                // Get the number of Female that will have a new litter
                double probaLitter = ((double)femaleNumberLitterPerYear[0])/12.;
                unsigned int nbLitter = nDraw(nbSurvive, probaLitter);

                // Get the number of new rabbits that will be added and add them 
                newRabbitLitter(allRabbit, nbLitter);

                // Put the number of females that didn't have a litter in newPos
                // and the number that did in newPos + 1
                allRabbit->allAdult[rabbitPosMature][newPos].numberOfRabbit += 
                    nbSurvive - nbLitter;
                allRabbit->allAdult[rabbitPosMature][newPos+1].numberOfRabbit += 
                    nbLitter;
            }else{

                // Put the rabbits that had survive at their new position
                allRabbit->allAdult[rabbitPosMature][newPos].numberOfRabbit = nbSurvive;
            }
        }
        
        allRabbit->allAdult[rabbitPosMature][rabbitPos].numberOfRabbit = 0;
    }
}


/**----------------------------------------------------------------------------
 *
 * @brief               Check all the rabbits inside of allRabbit
 * 
 * @param allRabbit     Pointer to a structure that contains all the possible
 *                      rabbits
 *
 ----------------------------------------------------------------------------*/
void checkAllRabbit(rabbitTable * allRabbit){
    
    // Save the Value of the 1 Month old rabbits to not lost them
    youngRabbit savedFirstMale = {allRabbit->allYoung[0].numberOfRabbit, 
                                  allRabbit->allYoung[0].sex};
    youngRabbit savedFirstFemale = {allRabbit->allYoung[1].numberOfRabbit, 
                                    allRabbit->allYoung[1].sex};

    // Reset the number of the 1 Month old rabbits so new rabbits can be added
    // in them
    allRabbit->allYoung[0].numberOfRabbit = 0;
    allRabbit->allYoung[1].numberOfRabbit = 0;

    // Check all the adult rabbits from the end to the beginning
    for(int i=allRabbit->sizeMature-1; i>=0; i--){

        for(int j=allRabbit->sizeAdult[i]-1; j>=0; j--){

            checkAdultRabbit(allRabbit, allRabbit->allAdult[i][j], 
                             (unsigned int) i, (unsigned int) j);
        }
    }

    // Check all the young rabbits from the end to the beginning
    for(unsigned int i=allRabbit->sizeYoung-1; i>1; i--){
        checkYoungRabbit(allRabbit, allRabbit->allYoung[i], i);
    }

    // Check the original 1 Month old rabbits
    checkYoungRabbit(allRabbit, savedFirstFemale, 1);
    checkYoungRabbit(allRabbit, savedFirstMale, 0);
}


/**----------------------------------------------------------------------------
 *
 * @brief                       Create or update the statistics of all the 
 *                              rabbit contained in allRabbit
 * 
 * @param stat                  Pointer to the structure of statistics of 
 *                              rabbits. If the pointer is NULL the structure 
 *                              will be created before being updated
 * @param allRabbit             Pointer to a structure that contains all 
 *                              the possible rabbits
 *
 * @return rabbitStatistic*     Pointer to the structure of statistics of 
 *                              rabbits
 *
 ----------------------------------------------------------------------------*/
rabbitStatistic * getStatistics(rabbitStatistic * stat, rabbitTable * allRabbit){
    // Allocate values if stat doesn't exist
    if(!stat){
        stat = calloc(1, sizeof(rabbitStatistic));
        stat->numberFemaleRabbitPerAge = calloc(maxSurvAge, sizeof(long unsigned int));
        stat->numberMaleRabbitPerAge = calloc(maxSurvAge, sizeof(long unsigned int));
        stat->numberFemaleRabbitPerLitter = calloc(femaleNumberLitterPerYear[1]+1, 
                                                   sizeof(long unsigned int));
    }

    // Reset old stat
    stat->numberAdultFemaleRabbit = 0;
    stat->numberAdultMaleRabbit = 0;
    stat->numberYoungFemaleRabbit = 0;
    stat->numberYoungMaleRabbit = 0;
    stat->numberYoungRabbit = 0;
    stat->numberAdultRabbit = 0;
    stat->totalRabbit = 0;
    for(unsigned int i=0; i<maxSurvAge; i++){
        stat->numberFemaleRabbitPerAge[i] = 0;
        stat->numberMaleRabbitPerAge[i] = 0;
    }
    for(unsigned int i=0; i<femaleNumberLitterPerYear[1]; i++){
        stat->numberFemaleRabbitPerLitter[i] = 0;
    }

    // Get all stat from young rabbit 
    for(unsigned int i=0; i<allRabbit->sizeYoung; i++){
        if(allRabbit->allYoung[i].numberOfRabbit != 0){
            stat->totalRabbit += allRabbit->allYoung[i].numberOfRabbit;
            stat->numberYoungRabbit += allRabbit->allYoung[i].numberOfRabbit;

            if(allRabbit->allYoung[i].sex == Male){
                stat->numberYoungMaleRabbit += allRabbit->allYoung[i].numberOfRabbit;
                stat->numberMaleRabbitPerAge[i/2] += allRabbit->allYoung[i].numberOfRabbit;
            }else{
                stat->numberYoungFemaleRabbit += allRabbit->allYoung[i].numberOfRabbit;
                stat->numberFemaleRabbitPerAge[i/2] += allRabbit->allYoung[i].numberOfRabbit;
            }
        }
    }
    // Get all stat from adult rabbit
    unsigned int adultAge = 0;
    for(unsigned int i=0; i<allRabbit->sizeMature; i++){
        adultAge = i+ageMaturityStart;
        for(unsigned int j=0; j<allRabbit->sizeAdult[i]; j++){
            if(j != 0 && allRabbit->allAdult[i][j].sex == Male){
                adultAge++;
            }

            if(allRabbit->allAdult[i][j].numberOfRabbit != 0){
                stat->totalRabbit += allRabbit->allAdult[i][j].numberOfRabbit;
                stat->numberAdultRabbit += allRabbit->allAdult[i][j].numberOfRabbit;

                if(allRabbit->allAdult[i][j].sex == Male){
                    stat->numberMaleRabbitPerAge[adultAge] += 
                        allRabbit->allAdult[i][j].numberOfRabbit;
                    stat->numberAdultMaleRabbit += 
                        allRabbit->allAdult[i][j].numberOfRabbit;
                }else{
                    stat->numberFemaleRabbitPerAge[adultAge] += 
                        allRabbit->allAdult[i][j].numberOfRabbit;
                    stat->numberAdultFemaleRabbit += 
                        allRabbit->allAdult[i][j].numberOfRabbit;
                    stat->numberFemaleRabbitPerLitter[allRabbit->allAdult[i][j].numberOfLitter] += 
                        allRabbit->allAdult[i][j].numberOfRabbit;
                }
            }
        }
    }

    return stat;
}


/**----------------------------------------------------------------------------
 *
 * @brief           Free all the statistics of all rabbits
 * 
 * @param stat      Address of a pointer to the rabbit statistics
 *
 ------------------------------------------------------------------------------*/
void freeRabbitStatistics(rabbitStatistic ** stat){
    free((*stat)->numberFemaleRabbitPerAge);
    free((*stat)->numberFemaleRabbitPerLitter);
    free((*stat)->numberMaleRabbitPerAge);
    free((*stat));

    *stat = NULL;
}


/**----------------------------------------------------------------------------
 * 
 * @brief               Run the simulation for the duration of months in input
 *                      and initialize it with nbYoungInit young rabbits. Once
 *                      the simulation has ended the stats are updated
 * 
 * @param duration      Duration in month that the simulation needs to run
 * @param nbYoungInit   Number of young rabbits the simulation needs to be
 *                      initialized with
 * @param allRabbit     Pointer to the structure of all the rabbits
 * @param stats         Pointer to the structure of the statistics
 * 
 ----------------------------------------------------------------------------*/
void runAllRabbitSimu(int duration, unsigned int nbYoungInit, 
    rabbitTable * allRabbit, rabbitStatistic * stats){

    // Reset old values in allRabbit
    for(unsigned int i = 0; i<allRabbit->sizeYoung; i++){
        allRabbit->allYoung[i].numberOfRabbit = 0;
    }
    for (unsigned int i = 0; i<allRabbit->sizeMature; i++){
        for(unsigned int j=0; j<allRabbit->sizeAdult[i]; j++){
            allRabbit->allAdult[i][j].numberOfRabbit = 0;
        }
    }

    // Add the number of rabbit to initialise the simulation
    addNewRabbit(allRabbit, nbYoungInit);

    // Run the simulation for the duration in input
    for(int i=1; i<duration+1; i++){

        checkAllRabbit(allRabbit);

    }

    // Get the statistics from all the rabbits
    getStatistics(stats, allRabbit);

}