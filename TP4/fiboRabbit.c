#include <stdio.h>
#include <stdlib.h>

#include "fiboRabbit.h"


/**----------------------------------------------------------------------------
 * 
 * @brief                   Function that simulate simply the reproduction of
 *                          rabbits.
 * 
 * @param   nbGeneration    Number of Generations in month the simulation
 *                          needs to run
 * @param   nbYoungRabbit   Total number of young rabbit couples that is left
 *                          at the end of the simulation. In input this number
 *                          will also be used for the initialisqtion of the
 *                          simulation
 * @param   nbAdultRabbit   Total number of adult rabbit couples that is left
 *                          at the end of the simulation. In input this number
 *                          will also be used for the initialisqtion of the
 *                          simulation
 * 
 ----------------------------------------------------------------------------*/
void simulateSimpleRabbit(int nbGeneration, long long unsigned int * nbYoungRabbit, 
    long long unsigned int * nbAdultRabbit){
    // Number of adult couples
    long long unsigned int nbAdultCouple = *nbAdultRabbit / 2;
    // Number of not mature couples
    long long unsigned int nbYoungCouple = *nbYoungRabbit / 2;

    for(int i=0; i<nbGeneration; i++){
        long long unsigned int oldNbAdultCouple = nbAdultCouple;
        nbAdultCouple += nbYoungCouple;
        nbYoungCouple = oldNbAdultCouple;
    }

    *nbAdultRabbit = nbAdultCouple*2;
    *nbYoungRabbit = nbYoungCouple*2;
}